#!/usr/bin/env python3

import rospy
import sys
import time
import roslaunch
from rospy.core import NullHandler


from std_msgs.msg import String, Bool
from mobile_base.msg import NavigationCmd
from agent.msg import ActionPlan

class Scheduler:

    def __init__(self):

        #set up subscribers
        self.nav_sub = rospy.Subscriber('/mobile_base/navigation/response', Bool, self.nav_callback)
        self.action_sub = rospy.Subscriber('/robotic_arm/action/response', Bool, self.action_callback)
        self.action_plan_sub = rospy.Subscriber('/scheduler/action/plan', ActionPlan, self.execute_actions)
        # self.spawn_sub = rospy.Subscriber('/brick/spawn/origin/response', Bool, self.action_callback)

        #set up publishers
        self.nav_pub = rospy.Publisher('/mobile_base/navigation/command', NavigationCmd, queue_size=1)
        self.action_pub = rospy.Publisher('/robotic_arm/action/command', String, queue_size=1)
        self.spawn_pub = rospy.Publisher('/brick/spawn/origin/command', Bool, queue_size=1)
        self.state_pub = rospy.Publisher('/construction/state/update', Bool, queue_size=1)

        #initialize class variables
        self.action_plan = []
        self.nav_cmd = NavigationCmd()
        self.action_cmd = String()
        self.spawn_cmd = Bool(True)
        self.state_cmd = Bool(True)

        #initialize wait flags
        self.nav_complete = True
        self.action_complete = True
        self.start_execution = False

        #start process execution
        self.execute()


        return


    def execute(self):
        #wait for an action plan to be sent to the scheduler
        while not self.start_execution:
            rospy.sleep(5)

        print("===================== SETTING UP APPLICATION =====================")
        #launch the nodes to handle the construction process
        self.launch_construction_nodes()
        #run the construciton process
        self.run()
        


    def launch_construction_nodes(self):
        #launch another launch files to launch all required nodes
        uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
        roslaunch.configure_logging(uuid)

        package = ['scheduler', 'construct.launch']
        roslaunch_file = roslaunch.rlutil.resolve_launch_arguments(package)

        self.parent = roslaunch.parent.ROSLaunchParent(uuid, roslaunch_file)
        self.parent.start()
        rospy.sleep(5)
 

    def nav_callback(self, data):
        #navigation is complete
        if data.data:
            #set flag to true
            self.nav_complete = data.data
            return




    def action_callback(self, data):
        #robotic arm action is complete
        if data.data:
            #set flag to true
            self.action_complete = data.data
            return



    def execute_actions(self,data):
        #listens to message from the agentnode to get the action plan

        self.action_plan = data.action_plan
        print("------------------------ GERNERATED ACTION PLAN ------------------------")
        print(self.action_plan)

        #set start execution - allows the application run 
        self.start_execution = True


    def run(self):
        #used to concatonate repeat navigation actions
        skip_count = 0

        #iterate over the action plan
        for i, action in enumerate(self.action_plan):
            action_type = action.split("_")[0]

            #if action have been concatonated skip over them
            if skip_count > 0:
                skip_count += -1
                continue

            #publish the action to the correct controller

            #navigation commands
            if action_type == "nav":
                self.nav_cmd.cmd = action
                self.nav_cmd.length = 1
                
                x = i
                #concatonate series of repeat actions
                while True:
                    x += 1
                    if self.action_plan[x] == action:
                        #incrments the distance to travel in the specified direction
                        self.nav_cmd.length += 1
                        skip_count += 1
                    else:
                        break

                #publish the action to the navigator node
                self.publish(self.nav_pub, self.nav_cmd)
                #block the app from running, set flag to false
                self.nav_complete = False

                #wait for action to complete
                while not self.nav_complete:
                    time.sleep(1)
                    
                continue


            #robotic arm actions
            if action_type == "drop" or action_type == "pick":
                #create command mesage
                self.action_cmd.data = action
                #publish the action to the actionexecuter node
                self.publish(self.action_pub, self.action_cmd)
                #block app form continuing
                self.action_complete = False

                #wait for action to complete
                while not self.action_complete:
                    time.sleep(1)

                if action_type == "pick":
                    #spawn new cube at state+1
                    self.publish(self.spawn_pub, self.spawn_cmd)
                else:
                    #update the state ID if the robot placed a brick
                    self.publish(self.state_pub, self.state_cmd)

                continue

        print('------------------------ CONSTRUCTION COMPLETE ------------------------')


    def publish(self, pub, cmd):
        #ensure connection exist before trying to publish
        while pub.get_num_connections() == 0:
            rospy.sleep(0.2)

        pub.publish(cmd) 

        



def main(args):
    # Instantiate your class
    # And rospy.init the entire node
    # rospy.init_node('Construction_Schedueler', anonymous=True)
  
    rospy.init_node('Construction_Schedueler', anonymous=True)
    cI = Scheduler()
    rospy.spin()
 
    # cI = Scheduler()
    # rospy.spin()



# Check if the node is executing in the main path
if __name__ == '__main__':
    main(sys.argv)