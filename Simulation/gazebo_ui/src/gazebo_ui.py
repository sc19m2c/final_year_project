#!/usr/bin/env python3

import rospy
import sys

from std_msgs.msg import String, Bool, Int32, Int32MultiArray, MultiArrayDimension
from gazebo_ros_link_attacher.srv import Attach, AttachRequest
from gazebo_msgs.srv import SpawnModel, SpawnModelRequest
from gazebo_msgs.msg import ModelState 
from gazebo_msgs.srv import SetModelState

import rospkg

import xml.etree.ElementTree as ET
from copy import deepcopy
import numpy as np

class GazeboUI:

    def __init__(self):

        #init publishers
        self.agent_pub = rospy.Publisher('/agent/sent/bp', Int32MultiArray, queue_size=1)
        self.agent_msg = Int32MultiArray()

        #init connection to gazebo 
        print("waiting for gazebo spawn service")
        self.spawn_srv = rospy.ServiceProxy('/gazebo/spawn_sdf_model', SpawnModel)
        self.spawn_srv.wait_for_service() 
        print("connected to gazebo spawn service")

        print("waiting for gazebo model state service")
        self.model_state_srv = rospy.ServiceProxy('/gazebo/set_model_state', SetModelState)
        self.model_state_srv.wait_for_service()
        print("connected to gazebo model state service")

        # rospy.sleep(5)      

        rospack = rospkg.RosPack()

        #init sdf objects in to memory
        path = rospack.get_path('gazebo_ui') + '/urdf/state_plane.sdf'
        tree = ET.parse(path)
        root = tree.getroot()
        self.plane = ET.tostring(root, encoding='utf8', method='xml').decode("utf-8") 

        path = rospack.get_path('gazebo_ui') + '/urdf/cube.sdf'
        tree = ET.parse(path)
        root = tree.getroot()
        self.cube = ET.tostring(root, encoding='utf8', method='xml').decode("utf-8") 

        #initialize user interface parameters
        self.current_cube = 0
        self.cube_location_x = 0
        self.cube_location_y = 0
        self.x_min = 2
        self.x_max = 4
        self.y_max = 0
        self.y_min = -4

        #intialize the blueprint in memory
        self.assigned_location = np.zeros((5,5), dtype="int32")

        #spawn the initial ui objects
        self.spawn_state_plane()
        self.spawn_new_cube()

        rospy.sleep(1)

        #start accepting user inputs
        self.user_inputs()

        return 


    def spawn_state_plane(self):
        #generate the spawn request for the plane
        #copy class plane object
        plane = deepcopy(self.plane)

        #create spawn message
        req = SpawnModelRequest()
        req.model_name = "state_plane"
        req.model_xml = plane
        #set location of plane
        req.initial_pose.position.x = 3.0
        req.initial_pose.position.y = -2.0
        req.initial_pose.position.z = -1
        #call gazebo to execute spawn
        self.spawn_srv.call(req)

        return

    

    def spawn_new_cube(self):
        #generate the spawn request for the plane
        #copy class cube object
        cube = deepcopy(self.cube)
        
        #assin a new model name
        brick_name = "brick_{}".format(self.current_cube)
        cube = cube.replace('MODELNAME', brick_name)
      
        #create spawn request
        req = SpawnModelRequest()
        req.model_name = brick_name
        req.model_xml = cube
        #set brick location
        req.initial_pose.position.x = 1
        req.initial_pose.position.y = 0
        req.initial_pose.position.z = 0.5
        #set class varriables location storing the location of the new cube 
        self.cube_location_x = 1
        self.cube_location_y = 0
        #call gazebo to execute spawn
        self.spawn_srv.call(req)

        return


    def user_inputs(self):
        #handle taking user inputs 

        x_change = 0
        y_change = 0

        #logs the CLI instructions
        print("===== CONTROLS =====")
        print("use awsd to move the cude")
        print("press q to complete")
        print("press e to place a cube")
        print("press h to view commands")

        #get input
        while True:
            print("enter command, h to view commands")
            cmd = input()

            #move cube
            if cmd == "a":
                #direction
                x_change = -1
                #check the command is valid
                if self.is_valid_command_x(x_change):
                    #adjust the class variable storiing the location of the cube
                    self.cube_location_x += x_change
                    #move the cube in gazebo
                    self.move_cube()
                else:
                    print("move out of bounds, cube must be within the green rectangle")
                
            #move cube
            elif cmd == "w":
                y_change = 1
                if self.is_valid_command_y(y_change):
                    self.cube_location_y += y_change
                    self.move_cube()
                else:
                    print("move out of bounds, cube must be within the green rectangle")

            #move cube
            elif cmd == "s":
                y_change = -1
                if self.is_valid_command_y(y_change):
                    self.cube_location_y += y_change
                    self.move_cube()
                else:
                    print("move out of bounds, cube must be within the green rectangle")

            #move cube  
            elif cmd == "d":
                x_change = 1
                if self.is_valid_command_x(x_change):
                    self.cube_location_x += x_change
                    self.move_cube()
                else:
                    print("move out of bounds, cube must be within the green rectangle")

            #ends the user interface, and returns the blueprint generated
            elif cmd == "q":
                break
            
            #set the current brick in posstion 
            elif cmd == "e":
                self.set_cube_location()

            #print the cli commands
            elif cmd == "h":
                self.print_commands()
            
            #handle the incorect inputs
            else:
                print("not a valid command")
                self.print_commands()

        #return the blueprint to the agent
        self.return_to_agent()

        return



    def move_cube(self):
        #move the cube to some location in gazebo
        #create model state message
        state_msg = ModelState()
        #identify the cube to move
        state_msg.model_name = 'brick_{}'.format(self.current_cube)
        #set new location of the cube
        state_msg.pose.position.x = self.cube_location_x
        state_msg.pose.position.y = self.cube_location_y
        state_msg.pose.position.z = 0.5
        state_msg.pose.orientation.x = 0
        state_msg.pose.orientation.y = 0
        state_msg.pose.orientation.z = 0
        state_msg.pose.orientation.w = 0

        #call gazebo to move the cude to the taget location
        self.model_state_srv.call(state_msg)

        return

    def is_valid_command_x(self, x_change):
        #checks if the command to move the cube was valid
        #prosed move target
        next_x = self.cube_location_x + x_change
        #checks move is within the valid range of locations
        if next_x >= self.x_min and next_x <= self.x_max:

            #checks the space is empty
            if self.assigned_location[abs(self.cube_location_y)][next_x] == 1:
                return False

            return True
        return False



    def is_valid_command_y(self, y_change):
        #checks if the command to move the cube was valid
        #prosed move target
        next_y = self.cube_location_y + y_change
        #checks move is within the valid range of locations
        if next_y >= self.y_min and next_y <= self.y_max:

            #checks the space is empty
            if self.assigned_location[abs(next_y)][self.cube_location_x] == 1:
                return False

            return True
        return False

    def print_commands(self):
        #prints all of the valid CLI commands
        print("use awsd to move the cude")
        print("press q to complete")
        print("press e to place a cube")
        print("press h to view commands")
        return


    def set_cube_location(self):
        #saves the current location of the cube into the blueprint

        # print(self.cube_location_y, self.cube_location_x)

        #use location of cube to determine the cell in blueprint, set it to 1
        self.assigned_location[abs(self.cube_location_y)][self.cube_location_x] = 1
        #incriment the cube number we are handling next
        self.current_cube += 1

        # print(self.assigned_location)

        #spawn a new cube to navigate
        self.spawn_new_cube()

        return


    def return_to_agent(self):
        #return the final blueprint the agentnode
        # print(self.assigned_location)

        #flatern a matrix to create an int32 multi array
        #set original data dimensions
        dim_0 = MultiArrayDimension()
        dim_0.label = "height"
        dim_0.size = 5
        dim_0.stride = 25

        dim_1 = MultiArrayDimension()
        dim_1.label = "width"
        dim_1.size = 5
        dim_1.stride = 5

        #add dimensions to message
        self.agent_msg.layout.dim.append(dim_0)
        self.agent_msg.layout.dim.append(dim_1)

        self.agent_msg.layout.data_offset = 0

        #move data from a matrix to array
        vec = []
        for row in self.assigned_location:
            vec += list(row)

        #add data to message
        self.agent_msg.data = vec

        #send the message to the agent
        self.agent_pub.publish(self.agent_msg)
    


def main(args):
    # Instantiate your class
    # And rospy.init the entire node
    rospy.init_node('User_Node', anonymous=True)
    cI = GazeboUI()
    rospy.spin()



# Check if the node is executing in the main path
if __name__ == '__main__':
    main(sys.argv)