#!/usr/bin/env python3

import rospy
import sys

from std_msgs.msg import String, Bool
from gazebo_ros_link_attacher.srv import Attach, AttachRequest
from construction_schedueler.srv import GetState, GetStateRequest, GetStateResponse
from inchworm_controller.srv import GrappleBrick, GrappleBrickResponse


class GrapleController:

    def __init__(self):

        # self.attach_srv = rospy.ServiceProxy('/link_attacher_node/attach', Attach)
        # self.attach_srv.wait_for_service()

        self.grapple_serv = rospy.Service("/inchworm/action/graple",GrappleBrick, self.test_server_listen )
        
        print("waiting for state server")
        self.get_state_srv = rospy.ServiceProxy('/construction/state/get', GetState)
        self.get_state_srv.wait_for_service()
        print("connected to state server")


        self.graple_link = "graple_2"
        self.target_model = "brick"

        # print("sleeping")
        # rospy.sleep(3)
        # self.test_state_connection()


        return 

    def test_state_connection(self):
        state_req = GetStateRequest(0)
        print(state_req)
        resp = self.get_state_srv.call(state_req)
        print(resp)

    

    def test_server_listen(self, data):
        print("incoming data:", data)
        state_req = GetStateRequest(0)
        print(state_req)
        resp = self.get_state_srv.call(state_req)
        return GrappleBrickResponse(True)







def main(args):
    # Instantiate your class
    # And rospy.init the entire node
    rospy.init_node('Graple_Controller', anonymous=True)
    cI = GrapleController()
    rospy.spin()



# Check if the node is executing in the main path
if __name__ == '__main__':
    main(sys.argv)