#!/usr/bin/env python3

import rospy
import sys

from std_msgs.msg import String, Bool
from gazebo_ros_link_attacher.srv import Attach, AttachRequest
from sim_state.srv import GetState, GetStateRequest, GetStateResponse
from grapple_controller.srv import GrappleBrick, GrappleBrickResponse


class GrapleController:

    def __init__(self):


        #initailize coneection to services
        rospy.logwarn('waiting for link attacher')
        self.attach_srv = rospy.ServiceProxy('/link_attacher_node/attach', Attach)
        self.attach_srv.wait_for_service()
        rospy.loginfo("connected to link attacher")

        rospy.logwarn("waiting for link detacher")
        self.detatach_srv = rospy.ServiceProxy('/link_attacher_node/detach',Attach)
        self.detatach_srv.wait_for_service()
        rospy.loginfo("connected to link detacher")


        rospy.logwarn("waiting for state srv")
        self.get_state_srv = rospy.ServiceProxy('/construction/state/get', GetState)
        self.get_state_srv.wait_for_service()
        rospy.loginfo("connected to state srvr")

        #set up server
        self.grapple_serv = rospy.Service("/robotic_arm/action/graple",GrappleBrick, self.grapple_callback )

        #name of commecting links
        self.graple_link = "graple_2"
        self.target_model = "brick_"

        #track if a brick is attached to the robot
        self.is_attached = False
        return 


    def grapple_callback(self, data):

        #create message object
        state_req = GetStateRequest(0)
        
        #identify the current brick
        brick_id = self.get_state_srv.call(state_req)
        brick_name = self.target_model + str(brick_id.state)

        #create request for attachers
        req = AttachRequest()
        req.model_name_1 = "final_robot"
        req.link_name_1 = self.graple_link
        req.model_name_2 = brick_name
        req.link_name_2 = "link"

        if self.is_attached:
            #detatch the brick if the robot is holding it 
            self.detatach_srv.call(req)
        else:
            #attach the brick if the robot is not holding it 
            self.attach_srv.call(req)

        #update the state is the robot is now holdin or not
        self.is_attached = not self.is_attached

        #return to agentnode
        return GrappleBrickResponse(True)

    
    # def release_callback(self, data):

    #     state_req = GetStateRequest()
    #     state_req.inc = 0

    #     brick_id = self.get_state_srv.call(state_req)
    #     brick_name = data.data + "_" + str(brick_id.state)

    #     req = AttachRequest()
    #     req.model_name_1 = "final_robot"
    #     req.link_name_1 = self.graple_link
    #     req.model_name_2 = brick_name
    #     req.link_name_2 = "link"

    #     self.attach_srv.call(req)

    #     return 




def main(args):
    # Instantiate your class
    # And rospy.init the entire node
    rospy.init_node('Graple_Controller', anonymous=True)
    cI = GrapleController()
    rospy.spin()



# Check if the node is executing in the main path
if __name__ == '__main__':
    main(sys.argv)