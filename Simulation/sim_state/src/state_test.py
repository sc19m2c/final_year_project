#!/usr/bin/env python3

import rospy
import sys

from std_msgs.msg import String, Bool, Int32

from sim_state.srv import GetState, GetStateResponse


class State:

    def __init__(self):

        #set up subscription
        self.update_sub = rospy.Subscriber('/construction/state/update', Int32, self.update_callback)

        #set up service server
        self.get_state_serv = rospy.Service("/construction/state/get",GetState, self.return_state_callback )


        self.current_state = Int32(0)

        return 

    
    def return_state_callback(self, data):
        return GetStateResponse(self.current_state.data)


    def update_callback(self,data):
        print(self.current_state.data)
        print(data.data)
        self.current_state.data = data.data
        return 
    


def main(args):
    # Instantiate your class
    # And rospy.init the entire node
    rospy.init_node('sim_state', anonymous=True)
    cI = State()
    rospy.spin()



# Check if the node is executing in the main path
if __name__ == '__main__':
    main(sys.argv)