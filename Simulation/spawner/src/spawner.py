#!/usr/bin/env python3

import rospy
import sys
from copy import deepcopy

from std_msgs.msg import String, Bool
from gazebo_msgs.srv import SpawnModel, SpawnModelRequest
from sim_state.srv import GetState, GetStateRequest, GetStateResponse

import xml.etree.ElementTree as ET
import rospkg

class Spawner:


    def __init__(self):
        
        #intialize connection to services
        print("waiting for gazebo spawn service")
        self.spawn_srv = rospy.ServiceProxy('/gazebo/spawn_sdf_model', SpawnModel)
        self.spawn_srv.wait_for_service() 
        print("connected to gazebo spawn service")

        print("waiting for state service")
        self.get_state_srv = rospy.ServiceProxy('/construction/state/get', GetState)
        self.get_state_srv.wait_for_service() 
        print("connected to state service")

        #set up subscribtions
        self.sched_sub = rospy.Subscriber('/brick/spawn/origin/command', Bool, self.sched_callback)


        #set up variables - create local instance of the brick sdf objcet
        rospack = rospkg.RosPack()
        path = rospack.get_path('spawner') + '/urdf/cube.sdf'
        tree = ET.parse(path)
        root = tree.getroot()
        self.base_cube = ET.tostring(root, encoding='utf8', method='xml').decode("utf-8") 

        self.model_prefix = "brick_"

        #spawn the initial brick to pikc up
        self.sched_callback(False)


        return


    def sched_callback(self, data):
        # generate spawn request
        request = self.generate_cube(data)
        # call spawn request
        self.spawn_srv.call(request)

        return 


    def generate_cube(self, data):
        #generate the spawn request
        state_req = GetStateRequest(0)
        brick_id = self.get_state_srv.call(state_req)
        brick_name = self.model_prefix + str(brick_id.state + (1 if data else 0))

        #copy class brick object and change the model name      
        cube = deepcopy(self.base_cube)
        cube = cube.replace('MODELNAME', brick_name)

        #create spwan model request
        req = SpawnModelRequest()
        req.model_name = brick_name
        req.model_xml = cube

        #set the location of the cube
        req.initial_pose.position.x = 0
        req.initial_pose.position.y = 1
        req.initial_pose.position.z = 0.5

        #return the cube
        return req



def main(args):
    # Instantiate your class
    # And rospy.init the entire node
    rospy.init_node('Brick_Spawner', anonymous=True)
    cI = Spawner()
    rospy.spin()



# Check if the node is executing in the main path
if __name__ == '__main__':
    main(sys.argv)