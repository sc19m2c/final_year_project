# INSTALATION

## Non ROS users

install ROS noetic and follow the tutorial on setting up the work space

install = http://wiki.ros.org/noetic/Installation

workspace guide = http://wiki.ros.org/catkin/Tutorials/create_a_workspace

## Compile the code
```
cd ~/catkin_ws/src 
git clone https://gitlab.com/sc19m2c/final_year_project.git 
cd ~/catkin_ws 
source ./devel/setup.sh 
catkin_make 
```
# User Manual

## Run the aplication, automatically generate a blueprint
```
roslaunch scheduler init.launch plan:=auto
```
## Run the aplication, launch user interface to create a blueprint

- The CLI provides instruction, launch the program and wait for propt
- Navigate the brick (a,w,s,d)
- Set the current brick location as a blueprint location (e)
- Complete the blueprint and start constructing (q)
- Print out the instructions (h)
```
roslaunch scheduler init.launch plan:=user
```

# Video Demonstrations

https://www.youtube.com/watch?v=GFeiet3BUto
