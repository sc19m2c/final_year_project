#!/usr/bin/env python3

import rl_agent
import rospy
import sys
import roslaunch
from rospy.core import NullHandler

from std_msgs.msg import String, Bool, Int32, Int32MultiArray

from agent.msg import ActionPlan

import numpy as np
import signal

import rl_agent as rl
import rl_agent.game.game as rlg
import rl_agent.learning.learn as rla


class AgentNode:

    def __init__(self, mode):

        #set up subscriber
        self.user_bp_sub = rospy.Subscriber('/agent/sent/bp', Int32MultiArray, self.user_bp_callback)
        
        #set up action plan publisher
        self.action_plan_pub = rospy.Publisher('/scheduler/action/plan', ActionPlan, queue_size=1)

        #command line arg - user or auto
        self.mode = mode

        #child luanch processes
        self.parent = None
        self.process = None

        #target blue print to construct
        self.blueprint = None

        # #load RL agent into memory
        # self.model = rla.DeepNet()

        #start the aplication
        self.run()

        return 


    def user_bp_callback(self, data):
        #determine the shape of the blueprint
        nRows = data.layout.dim[0].size
        nCols = data.layout.dim[1].size
        
        #reshape int32 array into matrix
        vec = np.array(data.data)
        blueprint = np.reshape(vec, (nRows,nCols))

        # create action plan
        self.kill()

        #load RL agent into memory
        self.model = rla.DeepNet()
        #user the RL agent to determine the action plan
        plan = self.model.gen_action_plan(blueprint)
        # print(plan)

        #send the action plan to the scheduler
        self.publish_action_plan(plan)



    def publish_action_plan(self, action_plan):
        # reusable function to publish the action plan to the scheduler
        # create actionplan message
        plan = ActionPlan()
        plan.action_plan = list(action_plan)
        # publish
        self.action_plan_pub.publish(plan)
        return

    


    
    def run(self):
        #process main funciton 
        if self.mode == "user":
            #start gazebo from process 
            uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
            roslaunch.configure_logging(uuid)

            package = ['gazebo_ui', 'gazebo_ui.launch']
            roslaunch_file = roslaunch.rlutil.resolve_launch_arguments(package)

            self.parent = roslaunch.parent.ROSLaunchParent(uuid, roslaunch_file)
            self.parent.start()

            #start gazeboUI process (user interface cli)
            node = roslaunch.core.Node("gazebo_ui", "gazebo_ui.py",  output="screen",)
            launch = roslaunch.scriptapi.ROSLaunch()
            launch.start()
            
            self.process = launch.launch(node)
        else:
            #load RL agent into memory
            self.model = rla.DeepNet()
            #create an action plan for a random blueprint
            plan = self.model.gen_action_plan()
            # print(plan)

            #send the action plan to the scheduler
            self.publish_action_plan(plan)
       



    def kill(self):
        #start killing the user interface
        print("WAIT while the user interface is closing")
        print("1) closing GazeboUI node")
        #kill gazeboUI
        self.process.stop()
        print("1) terminated gazeboUI =======================================")
        print("2) closing gazebo")
        #kill gazebo (slow)
        self.parent.shutdown()
        print("2) terminated gazebo =======================================")
        return

    


def main(args):

    # if args[1] == "user" or args[1] == "auto":
    print(args[1])
    if args[1] == "user" or args[1] == "auto":
        rospy.init_node('Agent_node', anonymous=True)
        cI = AgentNode(args[1])
        rospy.spin()
    else:
        print("usage: launch requires args 'plan:=<user or auto>'")




# Check if the node is executing in the main path
if __name__ == '__main__':
    main(sys.argv)