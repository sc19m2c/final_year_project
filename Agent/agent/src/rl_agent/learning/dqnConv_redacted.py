import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()
import tensorflow as tf2 
tf.logging.set_verbosity(tf.logging.ERROR)

class DQNetwork:
    def __init__(self, state_size, action_size, learning_rate, name='DQNetwork'):
        self.state_size = state_size
        self.action_size = action_size
        self.learning_rate = learning_rate

        with tf.variable_scope(name):
        
            print([None, *state_size, 1])
            self.inputs_ = tf.placeholder(tf.float32, [None, *state_size], name="inputs")
            self.actions_ = tf.placeholder(tf.float32, [None, *action_size], name="actions_")
            self.ISWeights_ = tf.placeholder(tf.float32, [None,1], name='IS_weights')
            
            self.target_Q = tf.placeholder(tf.float32, [None], name="target")

            """
            input layer
            """
            self.conv1 = tf.layers.conv2d(inputs = self.inputs_,
                                         filters = 32,
                                         kernel_size = [2,2],
                                         strides = [1,1],
                                         padding = "VALID",
                                         kernel_initializer=tf2.initializers.GlorotUniform(),
                                         name = "conv1")

            self.conv1_out = tf.nn.elu(self.conv1, name="conv1_out")

            """
            second layer layer
            """
            self.conv2 = tf.layers.conv2d(inputs = self.conv1_out,
                                 filters = 64,
                                 kernel_size = [2,2],
                                 strides = [1,1],
                                 padding = "VALID",
                                 kernel_initializer=tf2.initializers.GlorotUniform(),
                                 name = "conv2")

            self.conv2_out = tf.nn.elu(self.conv2, name="conv2_out")

            """
            third layer layer
            """
            self.conv3 = tf.layers.conv2d(inputs = self.conv2_out,
                                 filters = 128,
                                 kernel_size = [1,1],
                                 strides = [1,1],
                                 padding = "VALID",
                                kernel_initializer=tf2.initializers.GlorotUniform(),
                                 name = "conv3")

            self.conv3_out = tf.nn.elu(self.conv3, name="conv3_out")
            
            
            self.flatten = tf.layers.flatten(self.conv3_out)

            """
            value fc layer
            """
            self.value_fc = tf.layers.dense(inputs=self.flatten,
                                      units=512,
                                      activation=tf.nn.elu,
                                      kernel_initializer=tf2.initializers.GlorotUniform(),
                                      name="value_fc")

            """
            value layer
            """
            self.value = tf.layers.dense(inputs=self.value_fc,
                                          kernel_initializer=tf2.initializers.GlorotUniform(),
                                          units=1,
                                          activation=None,
                                          name = "value")

            """
            action fc layer
            """
            self.action_fc = tf.layers.dense(inputs=self.flatten,
                                      units=512,
                                      activation=tf.nn.elu,
                                      kernel_initializer=tf2.initializers.GlorotUniform(),
                                      name="action_fc")

            """
            action layer
            """
            self.action = tf.layers.dense(inputs=self.action_fc,
                                          kernel_initializer=tf2.initializers.GlorotUniform(),
                                          units=self.action_size[0],
                                          activation=None,
                                          name="action")

            """
            output layer layer
            """
            # special agregation layers dueling network
            self.output = self.value + tf.subtract(self.action, tf.reduce_mean(self.action, axis=1, keepdims=True))

            # Q is our predicted Q value.
            self.Q = tf.reduce_sum(tf.multiply(self.output, self.actions_), axis=1)
            
            # The loss is modified because of PER 
            self.absolute_errors = tf.abs(self.target_Q - self.Q)
            
            self.loss = tf.reduce_mean(self.ISWeights_ * tf.squared_difference(self.target_Q, self.Q))

            self.optimizer = tf.train.RMSPropOptimizer(self.learning_rate).minimize(self.loss)
