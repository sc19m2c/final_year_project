#!/usr/bin/env python3
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

# try:
from learning.dqnConv import DQNetwork
from learning.storage import Memory
from util.logger import Logger
from game.game import Game
# except:
#     print('crashed =======================')

import random

import numpy as np
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()
tf.logging.set_verbosity(tf.logging.ERROR)
import time
import rospkg

class DeepNet:

    game:Game = None
    actions = []
    dqn:DQNetwork = None
    t_dqn:DQNetwork = None
    memory:Memory = None

    state_size = None      
    action_size = None    

    ### Hyper parameters ------------------------------------------------------------------------------------------------------------------------------------------
    alpha =  0.001 # learning rate
    epochs = 2
    training_eps = 100000        
    game_length = 200 
    mb_size = 64             
    K_update = 5000 #update rate of the target network
    K = 0
    exp_start = 1.0           
    exp_min = 0.01           
    exp_decay = 0.000035 #decay of eps greedy policy
    training_step = 0  
    gamma = 0.95 #look forward of the belman equation, discount rate              
    pretrain_eps = 100000   
    mem_capacity = 100000

         
    def __init__(self):
        #create instacnes for utulity functions
        self.logger = Logger()
        
        tf.reset_default_graph()

        #create instacnes for training
        self.create_game()
        self.memory = Memory(self.mem_capacity)
        self.dqn = DQNetwork(self.state_size, self.action_size, self.alpha, name="DQNetwork")
        self.t_dqn = DQNetwork(self.state_size, self.action_size, self.alpha, name="TargetNetwork")
  
        return

    def create_game(self):
        #creates the environment for the agent to operate within
        self.game = Game((5,5))
        #determine training variables depending on the environment
        self.actions = self.game.actions
        self.action_size = [len(self.actions)]
        self.state_size = self.game.get_state().shape

        return


    def copy_weights(self):
        #used for copying weight from the trainng network to the target network, DOUBLE
        online = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "DQNetwork")
        offline = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, "TargetNetwork")
        ops = []
        for on,off in zip(online,offline):
            ops.append(off.assign(on))
        return ops


    def eps_greedy(self, sess):
        #implimentation of the epsilon greedy policy
        # get randon munber between 0-1
        eet = np.random.rand()
        # determine exploration probabilty 
        epsilon = self.exp_min + (self.exp_start - self.exp_min) * np.exp(-self.exp_decay * self.training_step)
        state = self.game.get_state()

        #explore 
        if (epsilon > eet):
            action = random.choice(self.actions)

        #greedy
        else:
            Qvalues = sess.run(self.dqn.output, feed_dict={self.dqn.inputs_: state.reshape((1, *state.shape))})
            choice = np.argmax(Qvalues)
            action = self.actions[int( choice)]

        return action, state


    def init_memory_data(self):
        #fill memory with training data
        self.game.reset()

        #pretraining epsiodes
        for i in range(self.pretrain_eps):
            self.game.reset()
            while True:
                #do a randon actios
                state = self.game.get_state()
                action = random.choice(self.actions)
                next_state, reward, done, is_success = self.game.do_action(action)

                if done:
                    #save the experience
                    next_state = np.zeros(state.shape)
                    experience = state, action, reward, next_state, done
                    self.memory.store(experience)
                    #start new epsiode
                    break

                else:
                    #save the experience
                    experience = state, action, reward, next_state, done
                    self.memory.store(experience)

        return

 
                

    def train(self, is_continue = False, addition_episodes =  0, time_episodes = 0):
        # traing the agent

        with tf.Session() as sess:
            self.model_saver = tf.train.Saver()

            # init network weights
            sess.run(tf.global_variables_initializer())
          
            # ensure Q = Q-
            update_target = self.copy_weights()
            sess.run(update_target)

            # training episodes
            for episode in range(self.training_eps):
                
                #initialise local epsoide variables
                loss = 0
                rewards = []

                self.game.reset()

                #playing the game
                for i in range(self.game_length):
                    #updates model parameters
                    self.K += 1
                    self.training_step += 1

                    # choose an action
                    action, state = self.eps_greedy(sess)
                    # perform the action in the environment
                    next_state, reward, done, is_success = self.game.do_action(action)
                    #store the rewards
                    rewards.append(reward)
                    # episode is complete
                    if done:
                        # terminal results in all 0 state
                        next_state = np.zeros(state.shape, dtype=np.int)
                        total_reward = np.sum(rewards)
                        # logs epsiode data
                        self.logger.log(episode, total_reward, loss)
                        # save the experience
                        experience = state, action, reward, next_state, done
                        self.memory.store(experience)
                        break

                    else:
                        #save the excerience
                        experience = state, action, reward, next_state, done
                        self.memory.store(experience)

                    # do the network optimization
                    loss = self.fit(sess,loss)

                    #update the target network
                    if self.K > self.K_update:
                        update_target = self.copy_weights()
                        sess.run(update_target)
                        self.K = 0
                        print("Q- wieghts updates ======================")

                # save the model for later usage
                if episode % 100 == 0:
                    self.model_saver.save(sess, "./models/model.ckpt")
                    print("model saved ==============================")

        return



    def fit(self, sess, loss):
     
        for i in range(self.epochs):
   
            tree_idx, batch, ISWeights_mb = self.memory.sample(self.mb_size)
            states_mb = np.array([each[0][0] for each in batch], ndmin=3)
            actions_mb = np.array([each[0][1] for each in batch])
            rewards_mb = np.array([each[0][2] for each in batch])
            next_states_mb = np.array([each[0][3] for each in batch], ndmin=3)
            dones_mb = np.array([each[0][4] for each in batch])

            target_Qs_batch = []
            
            q_next_state = sess.run(self.dqn.output, feed_dict={self.dqn.inputs_: next_states_mb})
            q_target_next_state = sess.run(self.t_dqn.output, feed_dict = {self.t_dqn.inputs_: next_states_mb})

            for i in range(0, len(batch)):
                terminal = dones_mb[i]
      
                action = np.argmax(q_next_state[i])

                if terminal:
                    target_Qs_batch.append(rewards_mb[i])
                    
                else:
                    target = rewards_mb[i] + self.gamma * q_target_next_state[i][action]
                    target_Qs_batch.append(target)

            targets_mb = np.array([each for each in target_Qs_batch])
        
            _, loss, absolute_errors = sess.run([self.dqn.optimizer, self.dqn.loss, self.dqn.absolute_errors],
                                feed_dict={self.dqn.inputs_: states_mb,
                                        self.dqn.target_Q: targets_mb,
                                        self.dqn.actions_: actions_mb,
                                        self.dqn.ISWeights_: ISWeights_mb})

            self.memory.batch_update(tree_idx, absolute_errors)
            
        return loss


    def test(self, test_episodes):
        # used to gather metrics during the training process
        with tf.Session() as sess:
            #load the saved model
            rospack = rospkg.RosPack()
            path = rospack.get_path('agent') + '/src/rl_agent/'
            tf.train.Saver().restore(sess, path+"/models/model.ckpt")
            #count success
            successes = 0
            #test episoded
            for i in range(test_episodes):
                self.game.reset()
                #play the game
                for j in range(self.game_length):
                    #follow the policy
                    state = self.game.get_state()
                    Qvalues = sess.run(self.dqn.output, feed_dict={self.dqn.inputs_: state.reshape((1, *state.shape))})
                    choice = np.argmax(Qvalues)
                    action = self.actions[int(choice)]
                    next_state, reward, done, is_success = self.game.do_action(action)

                    if done:
                        #count the successes
                        if is_success:
                            successes += 1
                        break


            print("Success rate: ", ((successes/test_episodes)*100), "%")
            print("Success view {} / {}".format(successes,test_episodes) )



    def gen_action_plan(self, blueprint=None):
        # using the trained model
        # store each action made
        action_plan = []
        blueprint = []
        with tf.Session() as sess:

            # Load the model
            rospack = rospkg.RosPack()
            path = rospack.get_path('agent') + '/src/rl_agent/'
            tf.train.Saver().restore(sess, path+"/models/model.ckpt")
   
            
            self.game.reset()
            #set the goal state as the blueprint
            if blueprint:
                self.game.current_state.set_goal_state(blueprint)

            state = self.game.get_state()

            if not blueprint:
                print(" --------------------------------- AUTO GENRATED BLUEPRINT ---------------------------------")
                print(state[:, :, 0]) 

            #play the game
            for j in range(self.game_length):
             
                # follow the policy
                state = self.game.get_state()
                Qvalues = sess.run(self.dqn.output, feed_dict={self.dqn.inputs_: state.reshape((1, *state.shape))})
                choice = np.argmax(Qvalues)
                action = self.actions[int(choice)]
                action_plan.append(self.game.action_controller.get_action_name(int(choice)))
                next_state, reward, done, is_success = self.game.do_action(action)

                if done:
                    break
           
        #return the action plan for construction
        return action_plan
