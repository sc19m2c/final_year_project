import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import tensorflow as tf
tf.logging.set_verbosity(tf.logging.ERROR)

class DQNetwork:
    def __init__(self, state_size, action_size, learning_rate, name='DQNetwork'):
        self.state_size = state_size
        self.action_size = action_size
        self.learning_rate = learning_rate

        with tf.variable_scope(name):
            # We create the placeholders
            # *state_size means that we take each elements of state_size in tuple hence is like if we wrote
            # [None, 84, 84, 4]
            self.inputs_ = tf.placeholder(tf.float32, [None, *state_size], name="inputs")
            self.actions_ = tf.placeholder(tf.float32, [None, *action_size], name="actions_")
            self.ISWeights_ = tf.placeholder(tf.float32, [None,1], name='IS_weights')
            
            # Remember that target_Q is the R(s,a) + ymax Qhat(s', a')
            self.target_Q = tf.placeholder(tf.float32, [None], name="target")

            """
            input layer
            """
            self.l1 = tf.layers.dense(inputs=self.inputs_,
                                      units=128,
                                      activation=tf.nn.elu,
                                      kernel_initializer=tf.contrib.layers.xavier_initializer(),
                                      name="l1")

            # self.l1_batchnorm = tf.layers.batch_normalization(self.l1,
            #                                                   training=True,
            #                                                   epsilon=1e-5,
            #                                                   name='batch_norm1')

            # self.l1_out = tf.nn.elu(self.l1_batchnorm, name="conv1_out")

            """
            second layer layer
            """
            self.l2 = tf.layers.dense(inputs=self.l1,
                                      units=256,
                                      activation=tf.nn.elu,
                                      kernel_initializer=tf.contrib.layers.xavier_initializer(),
                                      name="l2")

            # self.l2_batchnorm = tf.layers.batch_normalization(self.l2,
            #                                                   training=True,
            #                                                   epsilon=1e-5,
            #                                                   name='batch_norm2')

            # self.l2_out = tf.nn.elu(self.l2_batchnorm, name="conv2_out")

            """
            third layer layer
            """
            self.l3 = tf.layers.dense(inputs=self.l2,
                                      units=256,
                                      activation=tf.nn.elu,
                                      kernel_initializer=tf.contrib.layers.xavier_initializer(),
                                      name="l3")

            self.l4 = tf.layers.dense(inputs=self.l3,
                                      units=128,
                                      activation=tf.nn.elu,
                                      kernel_initializer=tf.contrib.layers.xavier_initializer(),
                                      name="l4")

            # self.l5 = tf.layers.dense(inputs=self.l4,
            #                           units=512,
            #                           activation=tf.nn.elu,
            #                           kernel_initializer=tf.contrib.layers.xavier_initializer(),
            #                           name="l5")

            # self.l6 = tf.layers.dense(inputs=self.l5,
            #                           units=256,
            #                           activation=tf.nn.elu,
            #                           kernel_initializer=tf.contrib.layers.xavier_initializer(),
            #                           name="l6")

            # self.l7 = tf.layers.dense(inputs=self.l6,
            #                           units=128,
            #                           activation=tf.nn.elu,
            #                           kernel_initializer=tf.contrib.layers.xavier_initializer(),
            #                           name="l7")

            # ------------------------------------------------------------

            """
            value fc layer
            """
            self.value_fc = tf.layers.dense(inputs=self.l4,
                                      units=64,
                                      activation=tf.nn.elu,
                                      kernel_initializer=tf.contrib.layers.xavier_initializer(),
                                      name="value_fc")

            """
            value layer
            """
            self.value = tf.layers.dense(inputs=self.value_fc,
                                          kernel_initializer=tf.contrib.layers.xavier_initializer(),
                                          units=1,
                                          activation=None,
                                          name = "value")

            """
            action fc layer
            """
            self.action_fc = tf.layers.dense(inputs=self.l4,
                                      units=64,
                                      activation=tf.nn.elu,
                                      kernel_initializer=tf.contrib.layers.xavier_initializer(),
                                      name="action_fc")

            """
            action layer
            """
            self.action = tf.layers.dense(inputs=self.action_fc,
                                          kernel_initializer=tf.contrib.layers.xavier_initializer(),
                                          units=self.action_size[0],
                                          activation=None,
                                          name="action")

            # self.l4_batchnorm = tf.layers.batch_normalization(self.l4,
            #                                                   training=True,
            #                                                   epsilon=1e-5,
            #                                                   name='batch_norm4')

            # self.l4_out = tf.nn.elu(self.l4_batchnorm, name="conv4_out")

            """
            output layer layer
            """
            # Agregating layer
            # Q(s,a) = V(s) + (A(s,a) - 1/|A| * sum A(s,a'))
            self.output = self.value + tf.subtract(self.action, tf.reduce_mean(self.action, axis=1, keepdims=True))

            # Q is our predicted Q value.
            self.Q = tf.reduce_sum(tf.multiply(self.output, self.actions_), axis=1)
            
            # The loss is modified because of PER 
            self.absolute_errors = tf.abs(self.target_Q - self.Q)
            
            self.loss = tf.reduce_mean(self.ISWeights_ * tf.squared_difference(self.target_Q, self.Q))

            self.optimizer = tf.train.RMSPropOptimizer(self.learning_rate).minimize(self.loss)
