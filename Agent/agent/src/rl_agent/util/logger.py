import csv  
import time
import rospkg

class Logger:

    
    def __init__(self):
        #open a new file and keep file open
        rospack = rospkg.RosPack()
        path = rospack.get_path('agent') + '/src/rl_agent/'
        self.filename = path + "/util/logs/training_session_" + str(int(time.time())) +".csv"
        self.file = open(self.filename, 'w', newline='')
        self.writer = csv.writer(self.file)
        return


    def log(self, episode, rewards, loss, eps):
        #append data to file
        self.writer.writerow([episode,rewards,loss, eps])
        self.file.flush()
        return