#!/usr/bin/env python3
# This first piece of skeleton code will be centred around
# extracting a specific colour and displaying the output

from trajectory_msgs.msg import JointTrajectoryPoint

import rospy
import yaml
import sys
import os
import pickle
from control_msgs.msg import FollowJointTrajectoryActionGoal
from moveit_msgs.msg import RobotState, Constraints, OrientationConstraint, PositionConstraint, BoundingVolume

import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

from copy import deepcopy

joint_dict = {
    "holding":[1.570879517773095, 1.3979893468019862, -0.5144660533028576, -1.0255596056019878, 1.5708],
    "pick_up":[3.145087300291703, 0.6481619577542742, 0.1616331855723694, -0.4851723050703474, 1.5708],
    "pick_left": [4.709308206083324, 0.6415375745965026, 0.16089969811309507, -0.4850976674047289, 1.5708],
    "pick_up_left":[3.9286278010706517, 0.38634242770757204, -0.07112890840520318, -0.4555315862670769, 3.925625707575172],

    "drop_left":[
        [4.714170326077311, 0.861154249314261, -0.32360237991339635, -1.184602061394442, 1.5708],
        [4.709308206083324, 0.6415375745965026, 0.16089969811309507, -0.4850976674047289, 1.5708]
    ],

    "drop_right":[
        [1.5730489173931144, 0.8593009012460437, -0.3229569128888996, -1.1819071856851593, 1.5708],
        [1.5705387314093924, 0.6409638527783894, 0.16084018098966968, -0.4801731021924637, 1.5708]
    ],

    "drop_up":[
        [3.1411379436858327, 0.8586383325254894, -0.3253332894879135, -1.1817378132052472, 1.5708],
        [3.145087300291703, 0.6481619577542742, 0.1616331855723694, -0.4851723050703474, 1.5708]
    ],

    "drop_down":[
        [-0.0026324300202911536, 0.860428678371108, -0.3254215236233653, -1.1829454003092794, 1.5708],
        [-0.003762867300308354, 0.6471215512021193, 0.16148459629457662, -0.4810874131513554, 1.5708]
    ],

    "drop_up_left":[
        [3.928031992036681, 0.6025249764997228, -0.5050761866160975, -1.1130783330336238, 3.929058515262092],
        [3.9286278010706517, 0.38634242770757204, -0.07112890840520318, -0.4555315862670769, 3.925625707575172]
    ],

    "drop_up_right":[
        [2.354982975633817, 0.6049093583854348, -0.5050914482523966, -1.103930575641449, 2.352597567390317],
        [2.3565881898725918, 0.38763908154975985, -0.07092290683933547, -0.4549894571956273, 2.355126454794644]
    ],

    "drop_down_left":[
        [-0.7850551302179724, 0.6057938643377274, -0.5027996306753509, -1.1128044890200421, -0.7804549611042157],
        [-0.787237586605214, 0.38276144429037584, -0.07351250581139751, -0.4533282116051996, -0.7870817405400298]
    ],

    "drop_down_right":[
        [0.7851836602076668, 0.6048614551358407, -0.5030780919849928, -1.1081398825637678, 0.7880868125452914],
        [0.7860888256076822, 0.38849440242577166, -0.07232823583373298, -0.45486057494595306, 0.7882821982240235]
    ]
}

class MoveitExecuter():
    def __init__(self):
        robot = moveit_commander.RobotCommander()

        group_names = robot.get_group_names()

        group_name = "arm"
        move_group = moveit_commander.MoveGroupCommander(group_name)

        robot = moveit_commander.RobotCommander()
        scene = moveit_commander.PlanningSceneInterface()    
        display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path', moveit_msgs.msg.DisplayTrajectory, queue_size=1)

        self.eef_link = move_group.get_end_effector_link()

        # self.execute_pose_goal(move_group)
        # self.execute_joint_goal(move_group)
        self.execute_joint_goal_loop(move_group)
     
        self.do_cartesian(move_group)

        return


    def execute_joint_goal(self, move_group):
        joint_goal = move_group.get_current_joint_values()
        joint_goal = joint_dict["holding"]
        plan = move_group.plan()
        move_group.go(joint_goal, wait=True)
        rospy.sleep(5)  

        return 

    def execute_joint_goal_loop(self, move_group):

        for targs in joint_dict["drop_right"]:
            joint_goal = move_group.get_current_joint_values()

            print(joint_goal)

            joint_goal = targs

            plan = move_group.plan()

            print(plan)

            move_group.go(joint_goal, wait=True)
            rospy.sleep(5)
            break

        return 

    
    def do_cartesian(self, move_group):

        waypoints = []

        wpose = move_group.get_current_pose().pose
        wpose.position.z -= 1
        waypoints.append(deepcopy(wpose))

        (plan, fraction) = move_group.compute_cartesian_path(
                                    waypoints,   # waypoints to follow
                                    0.1,        # eef_step
                                    0.0)


        move_group.execute(plan, wait=True)   


    # def demo_path_contraints(self, move_group):

    #     constraint = Constraints()
    #     constraint.name = "vertical"

    #     posConstraint = PositionConstraint()

    #     posConstraint.link_name = self.eef_link
    #     posConstraint.target_point_offset = [1 , 0 , 0]

    #     pc_volume = BoundingVolume()
    #     pc_volume.primitives.

    #     posConstraint.constraint_region.x = [0, 0.01]
    #     posConstraint.constraint_region.y = [0,0]
    #     posConstraint.constraint_region.z = [0,0]
    #     posConstraint.weight = 1

    #     posConstraint.header = move_group.get_current_pose().pose.header

    #     constraint.position_constraints.append(posConstraint)


    #     #move to position one

    #     joint_goal = move_group.get_current_joint_values()
    #     joint_goal = joint_dict["drop_right"][0]
    #     plan = move_group.plan()
    #     move_group.go(joint_goal, wait=True)
    #     rospy.sleep(1)

    #     # apply constraints

    #     move_group.set_path_contraints(constraint)

    #     # move to second goal

    #     joint_goal = move_group.get_current_joint_values()
    #     joint_goal = joint_dict["drop_right"][1]
    #     plan = move_group.plan()
    #     move_group.go(joint_goal, wait=True)
    #     rospy.sleep(1)




    #     return







    def execute_pose_goal(self, move_group):
        pose_target = geometry_msgs.msg.Pose()
        pose = move_group.get_current_pose()

        print(pose)
        
        move_group.set_goal_orientation_tolerance (0.005)
        move_group.set_goal_position_tolerance (0.005)
        
        pose_target.orientation.x =  -1
        pose_target.orientation.y =  0
        pose_target.orientation.z =  0
        pose_target.orientation.w =  0

        pose_target.position.x = 1
        pose_target.position.y = -1
        pose_target.position.z = 1.61
        move_group.set_pose_target(pose_target)

        plan = move_group.plan()

        move_group.go(wait=True)

        print("complete =============")

        print(move_group.get_current_joint_values())

     

        





def main(args):
    # Instantiate your class
    # And rospy.init the entire node
    moveit_commander.roscpp_initialize(sys.argv)
    rospy.init_node('My_Moveit_Executer', anonymous=True)
    cI = MoveitExecuter()
    rospy.spin()
    # Ensure that the node continues running with rospy.spin()
    # You may need to wrap rospy.spin() in an exception handler in case of KeyboardInterrupts

    # Remember to destroy all image windows before closing node


# Check if the node is executing in the main path
if __name__ == '__main__':
    main(sys.argv)