#!/usr/bin/env python3

import rospy
import sys
import moveit_commander
from std_msgs.msg import String, Bool
from grapple_controller.srv import GrappleBrick, GrappleBrickRequest



class ActionExecuter():

    def __init__(self) :
        #initialize moveit

        #set up service client
        print("waiting for grapple service")
        self.grapple_srv = rospy.ServiceProxy("/robotic_arm/action/graple", GrappleBrick)
        self.grapple_srv.wait_for_service()
        print("connected to grapple service")

        #set up variables
        self.grapple_req = GrappleBrickRequest(1)

        print("sleeping")
        rospy.sleep(3)
        self.test_grapple_connection()


        return

    

    def test_grapple_connection(self):
        print(self.grapple_srv.call(self.grapple_req))



def main(args):
    # Instantiate your class
    # And rospy.init the entire node
   
    rospy.init_node('Action_Executer', anonymous=True)
    cI = ActionExecuter()
    rospy.spin()



# Check if the node is executing in the main path
if __name__ == '__main__':
    main(sys.argv)