#!/usr/bin/env python3
# This first piece of skeleton code will be centred around
# extracting a specific colour and displaying the output



import rospy
import yaml
import sys
import os
import pickle
from control_msgs.msg import FollowJointTrajectoryActionGoal

import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

class MoveitExecuter():
    def __init__(self):


       
        group_name = "arm"
        self.box_name = "Box_demo"
        self.move_group = moveit_commander.MoveGroupCommander(group_name)
        self.eef_link = self.move_group.get_end_effector_link()
        print("end effector::", self.eef_link)
       
        self.robot = moveit_commander.RobotCommander()
        self.scene = moveit_commander.PlanningSceneInterface()

        
        
        display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path', moveit_msgs.msg.DisplayTrajectory, queue_size=1)

        self.log_frame()

        self.add_object()
        self.add_object_to_gripper()
        self.attack_object()
 

                
        return


    def log_frame(self):
        planning_frame = self.move_group.get_planning_frame()
        print ("============ Reference frame: %s" % planning_frame)
        print(self.robot.get_group_names())
        print(self.robot.get_planning_frame())


    def add_ground_plane(self):
        rospy.sleep(2)
        
        start = rospy.get_time()
        seconds = rospy.get_time()
        timeout = 100

        box_pose = geometry_msgs.msg.PoseStamped()

        #box_pose.header.frame_id = self.move_group.get_planning_frame()
        box_pose.header.frame_id = self.move_group.get_planning_frame()
        box_pose.pose.orientation.w = 1

        box_pose.pose.position.x = 0.0
        box_pose.pose.position.y = 0.0
        box_pose.pose.position.z = 0.0
        box_name = "ground_plane"


        self.scene.add_plane(box_name, box_pose)
        # self.scene.add_box(box_name, box_pose, size=(1, 1, 1))

        while (seconds - start < timeout) and not rospy.is_shutdown():
            # Test if the box is in attached objects
            attached_objects = self.scene.get_attached_objects([box_name])
            print(attached_objects)
            is_attached = len(attached_objects.keys()) > 0

            # Test if the box is in the scene.
            # Note that attaching the box will remove it from known_objects
            print(self.scene.get_known_object_names())
            is_known = box_name in self.scene.get_known_object_names()

            # Test if we are in the expected state
            if is_known:
                print("successfull :)")
                break

            # Sleep so that we give other threads time on the processor
            rospy.sleep(0.1)
            seconds = rospy.get_time()
  

    
    def add_object(self):
        rospy.sleep(2)
        
        start = rospy.get_time()
        seconds = rospy.get_time()
        timeout = 100

        box_pose = geometry_msgs.msg.PoseStamped()

        box_pose.header.frame_id = self.move_group.get_planning_frame()
        # box_pose.header.frame_id = 'world'
        box_pose.pose.orientation.w = 1

        x = 2.01
        y = 0

        box_pose.pose.position.x = x
        box_pose.pose.position.y = y
        box_pose.pose.position.z = 0.5
        box_name = "Box_demo_{}_{}".format(x,y)



        self.scene.add_box(box_name, box_pose, size=(1, 1, 1))

        while (seconds - start < timeout) and not rospy.is_shutdown():
            # Test if the box is in attached objects
            attached_objects = self.scene.get_attached_objects([box_name])
            print(attached_objects)
            is_attached = len(attached_objects.keys()) > 0

            # Test if the box is in the scene.
            # Note that attaching the box will remove it from known_objects
            print(self.scene.get_known_object_names())
            is_known = box_name in self.scene.get_known_object_names()

            # Test if we are in the expected state
            if is_known:
                print("successfull :)")
                break

            # Sleep so that we give other threads time on the processor
            rospy.sleep(0.1)
            seconds = rospy.get_time()


    
    def add_object_to_gripper(self):
        rospy.sleep(2)
        
        start = rospy.get_time()
        seconds = rospy.get_time()
        timeout = 100

        box_pose = geometry_msgs.msg.PoseStamped()

        box_pose.header.frame_id = self.eef_link
        # box_pose.header.frame_id = 'world'
        box_pose.pose.orientation.w = 1

        box_pose.pose.position.x = 0
        box_pose.pose.position.y = 0
        box_pose.pose.position.z = 0.5+0.6
        box_name = "Box_demo"



        self.scene.add_box(box_name, box_pose, size=(1, 1, 1))

        while (seconds - start < timeout) and not rospy.is_shutdown():
            # Test if the box is in attached objects
            attached_objects = self.scene.get_attached_objects([box_name])
            print(attached_objects)
            is_attached = len(attached_objects.keys()) > 0

            # Test if the box is in the scene.
            # Note that attaching the box will remove it from known_objects
            print(self.scene.get_known_object_names())
            is_known = box_name in self.scene.get_known_object_names()

            # Test if we are in the expected state
            if is_known:
                print("successfull :)")
                break

            # Sleep so that we give other threads time on the processor
            rospy.sleep(0.1)
            seconds = rospy.get_time()


    def attack_object(self):
        rospy.sleep(2)
        grasping_group = 'arm'
        touch_links = self.robot.get_link_names(group=grasping_group)
        print("touching links::", touch_links)

        self.scene.attach_box(self.eef_link, self.box_name, touch_links=[self.eef_link])


        start = rospy.get_time()
        seconds = rospy.get_time()
        timeout = 100

        while (seconds - start < timeout) and not rospy.is_shutdown():
            # Test if the box is in attached objects
            attached_objects = self.scene.get_attached_objects([self.box_name])
            is_attached = len(attached_objects.keys()) > 0
            print(attached_objects)
            # Test if the box is in the scene.
            # Note that attaching the box will remove it from known_objects
            print(self.scene.get_known_object_names())
            is_known = self.box_name in self.scene.get_known_object_names()

            # Test if we are in the expected state
            if is_attached:
                print("successfull :)")
                break

            # Sleep so that we give other threads time on the processor
            rospy.sleep(0.1)
            seconds = rospy.get_time()




def main(args):
    # Instantiate your class
    # And rospy.init the entire node
    moveit_commander.roscpp_initialize(sys.argv)
    rospy.init_node('My_Moveit_Executer', anonymous=True)
    cI = MoveitExecuter()
    rospy.spin()
    # Ensure that the node continues running with rospy.spin()
    # You may need to wrap rospy.spin() in an exception handler in case of KeyboardInterrupts

    # Remember to destroy all image windows before closing node


# Check if the node is executing in the main path
if __name__ == '__main__':
    main(sys.argv)