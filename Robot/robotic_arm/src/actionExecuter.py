#!/usr/bin/env python3

import rospy
import sys
import moveit_commander
from std_msgs.msg import String, Bool
from grapple_controller.srv import GrappleBrick, GrappleBrickRequest
from moveit_msgs.msg import RobotState, Constraints, OrientationConstraint, PositionConstraint

from copy import deepcopy


class ActionExecuter():

    def __init__(self) :
        #initialize moveit
  
        #set up moveit api's
        moveit_commander.roscpp_initialize(sys.argv)
        self.robot = moveit_commander.RobotCommander()
        self.group_name = "arm"
        self.move_group = moveit_commander.MoveGroupCommander(self.group_name)
        self.scene = moveit_commander.PlanningSceneInterface() 
        self.eef_link = self.move_group.get_end_effector_link()

        #set up subscribers
        self.sched_sub = rospy.Subscriber('/robotic_arm/action/command', String, self.sched_callback)
        #set up publishers
        self.sched_pub = rospy.Publisher('/robotic_arm/action/response', Bool, queue_size=1)

        #set up service client
        print("waiting for grapple srv")
        self.grapple_srv = rospy.ServiceProxy("/robotic_arm/action/graple", GrappleBrick)
        self.grapple_srv.wait_for_service()
        print("connected to grapple srv")

        #set up variables
        self.grapple_req = GrappleBrickRequest(1)
        self.sched_response = Bool()


        self.joint_dict = {
            "holding":[[1.570879517773095, 1.3979893468019862, -0.5144660533028576, -1.0255596056019878, 1.5708]],
            "pick_up":[[3.145087300291703, 0.6481619577542742, 0.1616331855723694, -0.4851723050703474, 1.5708]],
            "pick_left": [[4.709308206083324, 0.6415375745965026, 0.16089969811309507, -0.4850976674047289, 1.5708]],
            "pick_up_left":[[3.9286278010706517, 0.38634242770757204, -0.07112890840520318, -0.4555315862670769, 3.925625707575172]],

            "drop_left":[
                [4.714170326077311, 0.861154249314261, -0.32360237991339635, -1.184602061394442, 1.5708],
                # [4.709308206083324, 0.6415375745965026, 0.16089969811309507, -0.4850976674047289, 1.5708]
            ],

            "drop_right":[
                [1.5730489173931144, 0.8593009012460437, -0.3229569128888996, -1.1819071856851593, 1.5708],
                # [1.5705387314093924, 0.6409638527783894, 0.16084018098966968, -0.4801731021924637, 1.5708]
            ],

            "drop_up":[
                [3.1411379436858327, 0.8586383325254894, -0.3253332894879135, -1.1817378132052472, 1.5708],
                # [3.145087300291703, 0.6481619577542742, 0.1616331855723694, -0.4851723050703474, 1.5708]
            ],

            "drop_down":[
                [-0.0026324300202911536, 0.860428678371108, -0.3254215236233653, -1.1829454003092794, 1.5708],
                # [-0.003762867300308354, 0.6471215512021193, 0.16148459629457662, -0.4810874131513554, 1.5708]
            ],

            "drop_up_left":[
                [3.928031992036681, 0.6025249764997228, -0.5050761866160975, -1.1130783330336238, 3.929058515262092],
                # [3.9286278010706517, 0.38634242770757204, -0.07112890840520318, -0.4555315862670769, 3.925625707575172]
            ],

            "drop_up_right":[
                [2.354982975633817, 0.6049093583854348, -0.5050914482523966, -1.103930575641449, 2.352597567390317],
                # [2.3565881898725918, 0.38763908154975985, -0.07092290683933547, -0.4549894571956273, 2.355126454794644]
            ],

            "drop_down_left":[
                [-0.7850551302179724, 0.6057938643377274, -0.5027996306753509, -1.1128044890200421, -0.7804549611042157],
                # [-0.787237586605214, 0.38276144429037584, -0.07351250581139751, -0.4533282116051996, -0.7870817405400298]
            ],

            "drop_down_right":[
                [0.7851836602076668, 0.6048614551358407, -0.5030780919849928, -1.1081398825637678, 0.7880868125452914],
                # [0.7860888256076822, 0.38849440242577166, -0.07232823583373298, -0.45486057494595306, 0.7882821982240235]
            ]
        }

        return

    

    def sched_callback(self, data):

        print(self.joint_dict)

        #check action exists
        if not data.data in self.joint_dict:
            self.sched_response = False
            self.sched_respond()
            return

        joint_state = self.move_group.get_current_joint_values()

        #ensure robot is in holding starting state
        if not joint_state == self.joint_dict["holding"]:
            self.execute_joint_goal("holding")
        
        #move to target pose
        self.execute_joint_goal(data.data)

        #pick or release
        action_type = data.data.split("_")[0]

        if action_type == "drop":
            self.execute_vertical_cartesian()

        #attach brick to robot
        self.grapple_srv.call(self.grapple_req)

        #return to holding position
        self.execute_joint_goal("holding")

        #return success to schedueller
        self.sched_response.data = True
        self.sched_respond()

        return



    def execute_vertical_cartesian(self):
   

        waypoints = []

        wpose = self.move_group.get_current_pose().pose
        wpose.position.z -= 1
        waypoints.append(deepcopy(wpose))

        (plan, fraction) = self.move_group.compute_cartesian_path(
                                    waypoints,   # waypoints to follow
                                    0.1,        # eef_step
                                    0.0)


        self.move_group.execute(plan, wait=True)   


    def graple_response(self,data):
        return 
#!/usr/bin/env python3

import rospy
import sys
import moveit_commander
from std_msgs.msg import String, Bool
from grapple_controller.srv import GrappleBrick, GrappleBrickRequest
from moveit_msgs.msg import RobotState, Constraints, OrientationConstraint, PositionConstraint

from copy import deepcopy


class ActionExecuter():

    def __init__(self) :
        #initialize moveit
  
        #set up moveit api's
        moveit_commander.roscpp_initialize(sys.argv)
        self.robot = moveit_commander.RobotCommander()
        self.group_name = "arm"
        self.move_group = moveit_commander.MoveGroupCommander(self.group_name)
        self.scene = moveit_commander.PlanningSceneInterface() 
        self.eef_link = self.move_group.get_end_effector_link()

        #set up subscribers
        self.sched_sub = rospy.Subscriber('/robotic_arm/action/command', String, self.sched_callback)
        #set up publishers
        self.sched_pub = rospy.Publisher('/robotic_arm/action/response', Bool, queue_size=1)

        #set up service client
        print("waiting for grapple srv")
        self.grapple_srv = rospy.ServiceProxy("/robotic_arm/action/graple", GrappleBrick)
        self.grapple_srv.wait_for_service()
        print("connected to grapple srv")

        #set up variables
        self.grapple_req = GrappleBrickRequest(1)
        self.sched_response = Bool()

        #saved joint goals
        self.joint_dict = {
            "holding":[[1.570879517773095, 1.3979893468019862, -0.5144660533028576, -1.0255596056019878, 1.5708]],
            "pick_up":[[3.145087300291703, 0.6481619577542742, 0.1616331855723694, -0.4851723050703474, 1.5708]],
            "pick_left": [[4.709308206083324, 0.6415375745965026, 0.16089969811309507, -0.4850976674047289, 1.5708]],
            "pick_up_left":[[3.9286278010706517, 0.38634242770757204, -0.07112890840520318, -0.4555315862670769, 3.925625707575172]],

            "drop_left":[
                [4.714170326077311, 0.861154249314261, -0.32360237991339635, -1.184602061394442, 1.5708],
                # [4.709308206083324, 0.6415375745965026, 0.16089969811309507, -0.4850976674047289, 1.5708]
            ],

            "drop_right":[
                [1.5730489173931144, 0.8593009012460437, -0.3229569128888996, -1.1819071856851593, 1.5708],
                # [1.5705387314093924, 0.6409638527783894, 0.16084018098966968, -0.4801731021924637, 1.5708]
            ],

            "drop_up":[
                [3.1411379436858327, 0.8586383325254894, -0.3253332894879135, -1.1817378132052472, 1.5708],
                # [3.145087300291703, 0.6481619577542742, 0.1616331855723694, -0.4851723050703474, 1.5708]
            ],

            "drop_down":[
                [-0.0026324300202911536, 0.860428678371108, -0.3254215236233653, -1.1829454003092794, 1.5708],
                # [-0.003762867300308354, 0.6471215512021193, 0.16148459629457662, -0.4810874131513554, 1.5708]
            ],

            "drop_up_left":[
                [3.928031992036681, 0.6025249764997228, -0.5050761866160975, -1.1130783330336238, 3.929058515262092],
                # [3.9286278010706517, 0.38634242770757204, -0.07112890840520318, -0.4555315862670769, 3.925625707575172]
            ],

            "drop_up_right":[
                [2.354982975633817, 0.6049093583854348, -0.5050914482523966, -1.103930575641449, 2.352597567390317],
                # [2.3565881898725918, 0.38763908154975985, -0.07092290683933547, -0.4549894571956273, 2.355126454794644]
            ],

            "drop_down_left":[
                [-0.7850551302179724, 0.6057938643377274, -0.5027996306753509, -1.1128044890200421, -0.7804549611042157],
                # [-0.787237586605214, 0.38276144429037584, -0.07351250581139751, -0.4533282116051996, -0.7870817405400298]
            ],

            "drop_down_right":[
                [0.7851836602076668, 0.6048614551358407, -0.5030780919849928, -1.1081398825637678, 0.7880868125452914],
                # [0.7860888256076822, 0.38849440242577166, -0.07232823583373298, -0.45486057494595306, 0.7882821982240235]
            ]
        }

        return

    

    def sched_callback(self, data):
        #this process is run when the scheduler commans an action

        #check action exists
        if not data.data in self.joint_dict:
            self.sched_response = False
            self.sched_respond()
            return

        #get current configuration
        joint_state = self.move_group.get_current_joint_values()

        #ensure robot is in holding starting state
        if not joint_state == self.joint_dict["holding"]:
            self.execute_joint_goal("holding")
        
        #move to target pose
        self.execute_joint_goal(data.data)

        #pick or release
        action_type = data.data.split("_")[0]

        if action_type == "drop":
            self.execute_vertical_cartesian(-1)

        #attach/detach brick to robot
        self.grapple_srv.call(self.grapple_req)

        if action_type == "pick":
            self.execute_vertical_cartesian(1)

        #return to holding position
        self.execute_joint_goal("holding")

        #return success to schedueller
        self.sched_response.data = True
        self.sched_respond()

        return



    def execute_vertical_cartesian(self, z):
        #move the endefector strain up or down 
   
        #create waypoints for cartesian plan
        waypoints = []
        #determine the pose goal for the cartesian
        wpose = self.move_group.get_current_pose().pose
        #positive or negative 
        wpose.position.z += z
        waypoints.append(deepcopy(wpose))

        #create the trajectory
        (plan, fraction) = self.move_group.compute_cartesian_path(
                                    waypoints,   # waypoints to follow
                                    0.1,        # eef_step
                                    0.0)

        #execute the trajectory
        self.move_group.execute(plan, wait=True)   


    def graple_response(self,data):
        return 

    
    def execute_joint_goal(self, name):
        #move the robotic arm to some target configuration

        #execute each joint for to a target confguration 
        for joint_goal in self.joint_dict[name]:
            self.move_group.go(joint_goal, wait=True)
            rospy.sleep(1)

    
    # def reverse_joint_goal(self, name):

    #     joint_goal = self.joint_dict[name][0]
    #     self.move_group.go(joint_goal, wait=True)
    #     rospy.sleep(1)


    def sched_respond(self):
        #response to the schedueler - inform action is complete
        self.sched_pub.publish(self.sched_response)
        return





def main(args):
    # Instantiate your class
    # And rospy.init the entire node
   
    rospy.init_node('Action_Executer', anonymous=True)
    cI = ActionExecuter()
    rospy.spin()



# Check if the node is executing in the main path
if __name__ == '__main__':
    main(sys.argv)
    
    def execute_joint_goal(self, name):

        for joint_goal in self.joint_dict[name]:
            self.move_group.go(joint_goal, wait=True)
            rospy.sleep(1)

    
    def reverse_joint_goal(self, name):

        joint_goal = self.joint_dict[name][0]
        self.move_group.go(joint_goal, wait=True)
        rospy.sleep(1)


    def sched_respond(self):
        #response to the schedueler
        self.sched_pub.publish(self.sched_response)
        return





def main(args):
    # Instantiate your class
    # And rospy.init the entire node
   
    rospy.init_node('Action_Executer', anonymous=True)
    cI = ActionExecuter()
    rospy.spin()



# Check if the node is executing in the main path
if __name__ == '__main__':
    main(sys.argv)