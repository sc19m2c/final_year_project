#!/usr/bin/env python3

import rospy
import sys
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Twist
from std_msgs.msg import String, Bool

from mobile_base.msg import NavigationCmd


class Navigator:

    def __init__(self):

        #set up subscriber
        self.nav_cmd_sub = rospy.Subscriber('/mobile_base/navigation/command', NavigationCmd, self.sched_callback)
        self.odom_sub = rospy.Subscriber("/odom", Odometry, self.odom_callback)

        #set up publishers
        self.nav_resp_pub = rospy.Publisher('/mobile_base/navigation/response', Bool, queue_size=1)
        self.cmd_pub = rospy.Publisher("/cmd_vel", Twist, queue_size = 1)

        #set up publishrate
        self.rate = rospy.Rate(20)

        #set up variables----

        #valid actions
        self.commands = ["nav_left", "nav_right", "nav_up", "nav_down"]

        #location of mobile base
        self.local_x = 0
        self.local_y = 0

        #reusable message objects
        self.nav_cmd = Twist()
        self.nav_resp = Bool()

        #location of the goal
        self.goal = Point()

        return

    
    def sched_callback(self, data):
        # called the the scheduler wants to execute naivgation commands

        # print("navigator: cmd:", data)
        #get variables from command 
        cmd = data.cmd
        len = data.length

        #check for which action is commanded
        if cmd == self.commands[0]: #left
            return self.navigate_horizontal(-len)

        if cmd == self.commands[1]: #right
            return self.navigate_horizontal(len)

        if cmd == self.commands[2]: #up
            return self.navigate_vertical(len)

        if cmd == self.commands[3]: #down
            return self.navigate_vertical(-len)

        
        #invalid action requested -- return false
        self.nav_resp.data = False
        self.publish(self.nav_resp_pub, self.nav_resp)

    
    def odom_callback(self,data):
        #update the location of the mobile base
        self.local_x = data.pose.pose.position.x
        self.local_y = data.pose.pose.position.y
        return


    def publish(self, pub, cmd):
        #ensure connection exist before trying to publish
        while pub.get_num_connections() == 0:
            rospy.sleep(0.2)
        
        pub.publish(cmd) 


    def navigate_horizontal(self,direction):
        #update the goal location given an action
        self.goal.x = round(self.local_x) + direction

        #navigate toward goal location until error is below threshold then stop
        run = True
        while run:
            #get the distance to target location
            diff = self.goal.x - self.local_x
           
            #distance is greater the error
            if abs(diff) > 0.001:
                #velocity calculation
                v = abs(diff*2)*direction
                if abs(v) < 0.015:
                    v = 0.015*direction/abs(direction) #min speed
                elif abs(v) > 1.0:
                    v = 1.0*direction/abs(direction)#max speed

                self.nav_cmd.linear.x = v 

            else:
                # print(abs(diff))
                #stop
                self.nav_cmd.linear.x = 0
                run = False

            #publish relocity to robot
            self.cmd_pub.publish(self.nav_cmd)
            self.rate.sleep()

        #inform the schedueler this was successful
        self.nav_resp.data = True
        self.publish(self.nav_resp_pub, self.nav_resp)

        return

    
    def navigate_vertical(self, direction):
        #update the goal location given an action
        self.goal.y = round(self.local_y) + direction

        #navigate toward goal location until error is below threshold then stop

        run = True
        while run:
            #get the distance to target location
            diff = self.goal.y - self.local_y
           
            #distance is greater the error
            if abs(diff) > 0.001:
                #velocity calculation
                v = abs(diff*2)*direction

                if abs(v) < 0.015:
                    v = 0.015*direction/abs(direction) #min speed
                elif abs(v) > 1.0:
                    v = 1.0*direction/abs(direction)#max speed

                self.nav_cmd.linear.y = v 

                print(diff, direction, self.nav_cmd.linear.y )
                print(abs(v))

            else:
                # print(abs(diff))
                #stop
                self.nav_cmd.linear.y = 0
                run = False

            self.cmd_pub.publish(self.nav_cmd)

            self.rate.sleep()

        #inform the schedueler this was successful
        self.nav_resp.data = True
        self.publish(self.nav_resp_pub, self.nav_resp)

        return




def main(args):
    # Instantiate your class
    # And rospy.init the entire node
    rospy.init_node('Mobile_Base_Navigation', anonymous=True)
    cI = Navigator()
    rospy.spin()



# Check if the node is executing in the main path
if __name__ == '__main__':
    main(sys.argv)