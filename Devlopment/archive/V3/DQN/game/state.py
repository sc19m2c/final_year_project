import random
import numpy as np

class Position:
    x = 0
    y = 0
    z = 0
    is_complete = 0

    def __init__(self, x=0, y=0, z=0, rand=False):

        self.x = x
        self.y = y
        self.z = z

        if rand: self.randomize(rand)
        return

    def as_inputs(self):
        return np.array([self.x, self.y, self.z, self.is_complete])

    def is_complete_(self):
        return self.is_complete == 1

    def set_complete(self):
        self.is_complete = 1

    def set_target(self):
        self.is_complete = 0 

    def randomize(self, range):
        min,max = range
        self.x = random.randint(min, max)
        self.y = random.randint(min, max)
        self.z = random.randint(min, max)

    def __eq__(self, other):
        return self.x == other.x\
            and self.y == other.y\
            and self.z == other.z\

    def __hash__(self):
        return hash(('x', self.x,
                     'y', self.y,
                     'z', self.z))
                     
    def __repr__(self):
        return "({}, {}, {})".format(self.x, self.y, self.z)

class State:

    r1:Position = None
    b1:Position = None
    g1:Position = None
    holding_location:Position = None

    def __init__(self, r1, b1, g1, holding_location):
        self.r1 = r1
        self.b1 = b1
        self.g1 = g1
        self.holding_location = holding_location
        return

    def as_inputs(self):
        goal_inputs = self.g1.as_inputs()
        inputs = np.concatenate((self.r1.as_inputs(), self.b1.as_inputs(), goal_inputs.flatten()), axis=0)

        return inputs

    def is_holding(self):
        return self.b1 == self.holding_location

    def set_holding(self):
        self.b1 = self.holding_location


    def __eq__(self, other):
        return self.r1 == other.r1\
            and self.b1 == other.b1\
            and self.g1 == other.g1

    def __hash__(self):
        return hash(('robot', self.r1,
                     'brick', self.b1,
                     'goal', self.g1,))

    def __repr__(self):
        print(self.r1, self.b1, self.g1, self.holding_location)
        return super().__repr__()

