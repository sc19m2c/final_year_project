import os.path
import sys
import random
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from copy import deepcopy

try:
    from game.state import State, Position
    from game.graphicalInterface import Graph
except Exception:
    from .state import State, Position
    from .graphicalInterface import Graph

import random
import numpy as np

"""
state representation
[0,0,0,1,0,0,0]

G = target
B = brick

actions = [
    left, right, pick up, drop down
] len = 4
"""


class Game:

    shape = (0,0)

    # the formation of the goal brick layout
    current_state:State = None
    graph:Graph = None

    #locations
    holding_locations:Position = None

    states = None

    actions = np.array([
        [1, 0, 0, 0, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0, 0, 0, 0],
        [0, 0, 0, 1, 0, 0, 0, 0],
        [0, 0, 0, 0, 1, 0, 0, 0],
        [0, 0, 0, 0, 0, 1, 0, 0],
        [0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 0, 0, 0, 1],
    ])

    def __init__(self, shape, with_graph=False):

        self.with_graph = with_graph

        self.shape = shape
        self.holding_location = Position(shape[0], shape[1], -1)
        self.reset()

        return

    def reset(self):

        if self.graph and self.with_graph: del self.graph

        target_location = Position(rand=(0,self.shape[0]-1))

        self.current_state = State(
            Position(rand=(0,4)),
            random.choice([Position(rand=(0,4)) ]),
            Position(rand=(0,4)),
            deepcopy(self.holding_location),
        )


        if self.with_graph:
            self.graph = Graph(self.shape,
                                current_state=self.current_state
                                )

            print(self.holding_location)
            print(self.graph.current_state)

        return

    def get_reward(self, action):

        if((action == self.actions[0]).all()):
            # print('left')
            if self.current_state.r1.x-1 >= 0:
                return (-2, False, False)
            else:
                return (-20, True, False)

        if((action == self.actions[1]).all()):
            # print('right')
            if self.current_state.r1.x + 1 < self.shape[1]:
                return (-2, False, False)
            else:
                return (-20, True, False)

        if((action == self.actions[2]).all()):
            # print('forward')
            if self.current_state.r1.y + 1 < self.shape[1]:
                return (-2, False, False)
                
            else:
                return (-20, True, False)


        if((action == self.actions[3]).all()):
            # print('backward')
            if self.current_state.r1.y - 1 >= 0 :
                return (-2, False, False)
                
            else:
                return (-20, True, False)

        if((action == self.actions[4]).all()):
            # print('up')
            if self.current_state.r1.z + 1 < self.shape[2]:
                return (-2, False, False)
                
            else:
                return (-20, True, False)


        if((action == self.actions[5]).all()):
            # print('down')
            if self.current_state.r1.z - 1 >= 0:
                return (-2, False, False)
                
            else:
                return (-20, True, False)

        #----

        if((action == self.actions[6]).all()):
            # print('pick')
            if not self.current_state.is_holding():
                if self.current_state.b1 == self.current_state.r1:
                    print("---------------------------------------------------------------------------- PICK [[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[[-----------------------]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]]")
                    return (50, False, False)
                else:
                    return (-10, True, False)
            else:
                return (-30, True, False)

        if((action == self.actions[7]).all()):
            # print('drop')
            if self.current_state.is_holding():
                if self.current_state.r1 == self.current_state.g1:
                    print("---------------------------------------------------------------------------- SUCCESS -----------------------")
                    return (100, True, True)
                else:
                    return (-10, True, False)
            else:
                return (-30, True, False)

        raise Exception('Incorrect actions parsed {}'.format(action))

    def update_state(self, action):

        if((action == self.actions[0]).all()):
            self.current_state.r1.x += -1

        if((action == self.actions[1]).all()):
            self.current_state.r1.x += 1
        
        if((action == self.actions[2]).all()):
            self.current_state.r1.y += 1
 
        if((action == self.actions[3]).all()):
            self.current_state.r1.y += -1

        if((action == self.actions[4]).all()):
            self.current_state.r1.z += 1
 
        if((action == self.actions[5]).all()):
            self.current_state.r1.z += -1

        if((action == self.actions[6]).all()):
            self.current_state.set_holding()

        if((action == self.actions[7]).all()):
            self.current_state.b1 = self.current_state.r1

        return


    def print_state(self):

        r = self.current_state.r1
        b = self.current_state.b1
        g = self.current_state.g1

        print('r:', r)
        print('b:', b)
        print('g:', g)

        return

    def do_action(self, action):

        if self.with_graph: start_state = deepcopy(self.current_state)

        reward, done, is_success = self.get_reward(action)

        if not done:
            self.update_state(action)

        print(reward)
        if self.with_graph:
            end_state = deepcopy(self.current_state)
            self.graph.add_move(start_state.r1, end_state.r1, self.current_state)

        state = self.get_state()
        return (state, reward, done, is_success)

    def get_state(self):
        return self.current_state.as_inputs()


def test():
    game = Game((5,5))

    game.print_state()

    print('-------------------')

    game.print_state()


if __name__ == "__main__":
    test()
