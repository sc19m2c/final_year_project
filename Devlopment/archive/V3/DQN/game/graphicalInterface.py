from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
from typing import List

try:
    from game.state import Position, State
except Exception:
    from .state import Position, State



class Graph:

    def __init__(self, shape, current_state):

        self.locations:List[Position] = [current_state.g1]
        self.robot_location = current_state.r1
        self.pickup_location = current_state.b1
        

        self.current_state:State = current_state
        self.moves = []
        self.shape = shape


        self.fig = plt.figure()
        self.ani = animation.FuncAnimation(self.fig, self.plot, interval=100)
    
        self.ax = self.fig.add_subplot(111, projection='3d')
      
        self.bx = self.fig.gca(projection='3d')

        return 

    def add_move(self, prev, next, state):
        self.current_state = state
        self.robot_location = next
        self.moves.append((prev,next))

    def show(self):
        plt.ion()     # turns on interactive mode
        plt.show()

    def plot(self, i):
        self.ax.clear()
        self.bx.clear()

        self.ax.scatter(0,0,self.shape[2], c='w', marker=",")
        self.ax.scatter(0,self.shape[1],0,c='w', marker=",")
        self.ax.scatter(self.shape[0],0,0,c='w', marker=",")

        self.ax.set_xlabel('X Label')
        self.ax.set_ylabel('Y Label')
        self.ax.set_zlabel('Z Label')
     
        self.plot_locations()
        self.plot_robot()
        self.plot_moves()
        #refactor need to plot the pick up location as this will move over time



    def plot_locations(self):
        xs,ys,zs,cs,ms = [],[],[],[],[]
        for target in self.locations:
            xs.append(target.x)
            ys.append(target.y)
            zs.append(target.z)
            cs.append('r' if target.is_complete == 0 else 'g')
            ms.append('o')

        self.ax.scatter(xs,ys,zs,c=cs,marker='o')

        return

    def plot_robot(self):

        c = 'r'
        m = 's'

        if len(self.locations) > 0:


            if self.robot_location == self.locations[0] and self.current_state.is_holding():
                c = 'g'
                m = '^'

            elif self.robot_location == self.pickup_location and not self.current_state.is_holding():
                c = 'b'
                m = '^'

            elif self.current_state.is_holding():
                c = 'g'
                m = 's'
            
            else:
                c = 'r'
                m = 's'


        self.ax.scatter(self.robot_location.x,
                        self.robot_location.y,
                        self.robot_location.z,c=c,marker=m)
        
        

        return

    def plot_moves(self):
        xs,ys,zs, us,vs,ws = [],[],[],[],[],[]

        for pair in self.moves:
            xs.append(pair[0].x)
            ys.append(pair[0].y)
            zs.append(pair[0].z)

            us.append((pair[1].x-pair[0].x))
            vs.append((pair[1].y-pair[0].y))
            ws.append((pair[1].z-pair[0].z))

        self.bx.quiver(xs, ys, zs, us, vs, ws, length=0.9, normalize=True, color='red')
        

        return





def test():
    locations = [
        Position(rand=(0,4)),
        Position(rand=(0,4)),
        Position(rand=(0,4))
    ]

    print(locations)
    robot_location = Position(rand=(0,4))

    graph = Graph(locations, robot_location)
    graph.moves = [
        (Position(1,1,1),Position(1,2,1)),
        (Position(1,2,1),Position(1,2,2)),
    ]
    # graph.plot()
    print("testing")
    graph.show()

    while True:
        graph.locations.append(Position(rand=(0,4)))
        action = input("Enter action:")
        if action == 'q':
            break
    return


if __name__ == "__main__":
    test()