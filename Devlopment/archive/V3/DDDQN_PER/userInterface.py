try:
    from game.game import Game
except Exception:
    from game import Game


def main():
    game = Game((5,5,5), with_graph=True)
    game.graph.show()
    
    while True:
        game.print_state()
            
        action = input("Enter action:")
        state, reward, done, is_success = game.do_action(game.actions[int(action)])

        if(done):
            break
    return


if __name__ == "__main__":
    main()
