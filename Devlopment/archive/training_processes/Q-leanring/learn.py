import numpy as np
import random


from game.game import Game

from metrics.metrics import Metrics 


class QLearn:

    game = None
    Q_table = None

    #hyper parameters ----
    
    train_eps = 10000      
    test_eps = 100    
    game_len = 100              

    alpha = 0.01        
    gamma = 0.99                 

    eps = 1.0                
    max_eps = 1.0            
    min_eps = 0.001           
    eps_decay = 0.01             

    metrics = Metrics()

    def __init__(self):
        return

    def create_game(self):
        self.game = Game(5)
        state_space = self.game.size*self.game.size*(self.game.size+1)
        self.Q_table = np.zeros((state_space, len(self.game.actions)))

        return


    def eps_greedy(self, Q, state):
        #choose action greedily
        if(random.uniform(0, 1) > self.eps):
            action = np.argmax(self.Q_table[state])
        #choose a randon action
        else:
            action = random.randint(0, len(self.game.actions)-1)

        return action



    def train(self):
        #for each training episode
        for episode in range(self.train_eps):
            #reset the game env
            self.game.reset()
            #identify the current state
            state = self.game.get_state_index()
            step = 0
            done = False

            #decay the eps greedy policy
            self.eps = self.min_eps + (self.max_eps - self.min_eps)*np.exp(-self.eps_decay*episode)

            rewards = 0

            #test every 1k eps
            if episode % 1000 == 0 or episode == self.train_eps-1:
                    self.play(metric_ep=episode)

            #for eact time step
            for step in range(self.game_len):
                #choose action
                action = self.eps_greedy(self.Q_table, state)
                #perform action
                new_state, reward, done, is_success = self.game.do_action(action)

                #do belman equation to update q table
                loss = (reward + self.gamma * np.max(self.Q_table[new_state]) - self.Q_table[state][action])
                q_value = self.Q_table[state][action]
                update = q_value + self.alpha * loss

                #update the qtable state action pair
                self.Q_table[state][action] = update

                rewards += reward

                if done is True:
                    #save the data
                    self.metrics.update_complteness(1 if is_success else 0, episode)
                    self.metrics.update_loss((loss), episode)
                    self.metrics.update_rewards(rewards, episode )
                    #end the epsiode if terminal
                    break


                state = new_state

        #save all the data
        self.metrics.save_all()

        return

    def play(self, test_episodes = 1000, metric_ep = None):

        #store successes and cofusion rate
        success_count = 0.00001
        confussion_count = 0.00001

        #each test epsidoe
        for episode in range(test_episodes):
            #reset the env
            self.game.reset()
            #get the current state
            state = self.game.get_state_index()
            done = False
         
            #for each time step
            for step in range(self.game_len):
                #choose action
                action = np.argmax(self.Q_table[state][:])
                #do the action
                new_state, reward, done, is_success = self.game.do_action(action)

                if done:
                    if is_success:
                        #count success
                        success_count += 1
                    break

                if step == self.game_len - 1:
                    #count confusion
                    confussion_count += 1

                state = new_state

        #save the data
        self.metrics.update_confusion((confussion_count/1000), metric_ep)
        self.metrics.update_success_rate((success_count/1000), metric_ep)



    def save_model(self):
        with open('test.npy', 'wb') as outfile:
            np.save(outfile, self.Q_table)
        return

    def load_model(self):
        with open('test.npy', 'rb') as model_file:
            self.Q_table = np.load(model_file)

        print(self.Q_table)
        return

    def print_model(self):
        print(self.Q_table)


def test():
    model = QLearn()
    model.create_game()
    print('-------------------')


if __name__ == "__main__":
    test()
