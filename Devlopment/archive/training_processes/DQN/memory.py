import numpy as np
from collections import deque


class Memory():
    def __init__(self, max_size):
        #data structure for memory - remove oldest experiences 
        self.mem = deque(maxlen=max_size)

    def add(self, experience):
        #add experience to memory
        self.mem.append(experience)

    def sample(self, batch_size):
        #samples a batch
        mem_size = len(self.mem)
        index = np.random.choice(np.arange(mem_size),size=batch_size,replace=False)

        return [self.mem[i] for i in index]


    def print(self, option="all"):

        print(self.mem[0])
            
