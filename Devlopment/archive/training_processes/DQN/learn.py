import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

from learning.dqn import DQNetwork
from learning.memory import Memory
from game.game import Game
import random
import tensorflow as tf
import numpy as np
from metrics.metrics import Metrics

tf.logging.set_verbosity(tf.logging.ERROR)

class DeepNet:

    game:Game = None
    actions = []
    dqn:DQNetwork = None
    memory:Memory = None

    state_size = [10]     

    action_size = [6] 

    # hyper params --
               
    alpha = 0.001    
    training_eps = 30000     
    epochs = 2
    game_len = 100           
    mb_size = 64
    exp_start = 1.0           
    exp_min = 0.01            
    exp_decay = 0.00005 
    training_step = 0           
    gamma = 0.95            
    pretrain_eps = 100000   
    mem_capacity = 100000         

    metrics = Metrics()

    def __init__(self):
        self.memory = Memory(max_size=self.mem_capacity)
        tf.reset_default_graph()
        self.create_game()
        self.dqn = DQNetwork(self.state_size, self.action_size, self.alpha)
        return

    def create_game(self):
        self.game = Game((5,5))
        self.actions = self.game.actions
        self.action_size = [len(self.actions)]
        self.state_size = [len(self.game.get_state())]
        return



    def eps_greedy(self, sess):
        #implimentation of the epsilon greedy policy
        # get randon munber between 0-1
        eet = np.random.rand()
        # determine exploration probabilty 
        epsilon = self.exp_min + (self.exp_start - self.exp_min) * np.exp(-self.exp_decay * self.training_step)
        state = self.game.get_state()

        #explore 
        if (epsilon > eet):
            action = random.choice(self.actions)

        #greedy
        else:
            Qvalues = sess.run(self.dqn.output, feed_dict={self.dqn.inputs_: state.reshape((1, *state.shape))})
            choice = np.argmax(Qvalues)
            action = self.actions[int( choice)]

        return action, state, epsilon

    def init_memory_data(self):
        

        #pretraining epsiodes
        for i in range(self.pretrain_eps):
            self.game.reset()
            while True:
                #do a randon actios
                state = self.game.get_state()
                action = random.choice(self.actions)
                next_state, reward, done, is_success = self.game.do_action(action)

                if done:
                    #save the experience
                    next_state = np.zeros(state.shape)
                    experience = (state, action, reward, next_state, done)
                    self.memory.add(experience)
                    #start new epsiode
                    break

                else:
                    #save the experience
                    experience = state, action, reward, next_state, done
                    self.memory.add(experience)

        return


    def train(self):

        saver = tf.train.Saver()

        with tf.Session() as sess:
            #init network vars
            sess.run(tf.global_variables_initializer())

            #for each episode
            for episode in range(self.training_eps):
                print("training epsiode ---- ", episode)
            
                loss = 0
                rewards = []

                #reset game env
                self.game.reset()
   
                #test every 1000 episode
                if episode % 1000 == 0 or episode == self.training_eps-1:
                    self.test(sess=sess, metric_ep=episode)
                    self.game.reset()

                #for each time step
                for step in range(self.game_len):
                    #incriment training step
                    self.training_step += 1
                    #choose actions
                    action, state, epsilon = self.eps_greedy(sess)
                    #do the action
                    next_state, reward, done, is_success = self.game.do_action(action)

                    rewards.append(reward)


                    if done:
                        #save results
                        self.metrics.update_complteness(self.game.get_completeness(), episode)
                        self.metrics.update_loss((loss), episode)
                        self.metrics.update_rewards(sum(rewards), episode )

                        next_state = np.zeros(state.shape, dtype=np.int)
                        #save experience
                        self.memory.add((state, action, reward, next_state, done))

                    else:
                        #save the experience
                        self.memory.add((state, action, reward, next_state, done))

                    #fit the networl
                    loss = self.fit(sess,loss, episode)

                    #go to next episode if terminal
                    if done:
                        break

                #save model every 100 eps
                if episode % 100 == 0:
                    saver.save(sess, "./models/model.ckpt")
    
        #save all data
        self.metrics.save_all()

        return

    def fit(self, sess, loss, episode):
        #optimize on the mini batch
        
        for i in range(self.epochs):
            batch = self.memory.sample(self.mb_size)
            states_mb = np.array([each[0] for each in batch], ndmin=2)
            actions_mb = np.array([each[1] for each in batch])
            rewards_mb = np.array([each[2] for each in batch])
            next_states_mb = np.array([each[3] for each in batch], ndmin=2)
            dones_mb = np.array([each[4] for each in batch])

            target_Qs_batch = []
            
            Qs_next_state = sess.run(self.dqn.output, feed_dict={self.dqn.inputs_: next_states_mb})

            for i in range(0, len(batch)):
                terminal = dones_mb[i]


                if terminal:
                    target_Qs_batch.append(rewards_mb[i])

                else:
                    target = rewards_mb[i] + self.gamma * np.max(Qs_next_state[i])
                    target_Qs_batch.append(target)

            targets_mb = np.array([each for each in target_Qs_batch])

            loss, _ = sess.run([self.dqn.loss, self.dqn.optimizer],
                                feed_dict={self.dqn.inputs_: states_mb,
                                            self.dqn.target_Q: targets_mb,
                                            self.dqn.actions_: actions_mb})

   

        return loss



    def test(self, sess, test_episodes = 1000, metric_ep = None):
        # used to gather metrics during the training process
        
        #count success
        success_count = 0
        confussion_count = 0
        #test episoded
        for i in range(test_episodes):
            print(i, "/", test_episodes, "  ===== TESTING")

            self.game.reset()
            #play the game
            for j in range(self.game_len):
                #follow the policy
                state = self.game.get_state()
                Qvalues = sess.run(self.dqn.output, feed_dict={self.dqn.inputs_: state.reshape((1, *state.shape))})
                choice = np.argmax(Qvalues)
                action = self.actions[int(choice)]
                next_state, reward, done, is_success = self.game.do_action(action)

                if done:
                    #count the successes
                    if is_success:
                        success_count += 1
                    break

                if j == self.game_len - 1:
                    confussion_count += 1

        self.metrics.update_confusion((confussion_count/1000), metric_ep)
        self.metrics.update_success_rate((success_count/1000), metric_ep)


    def save_model(self):
        with open('test.npy', 'wb') as outfile:
            np.save(outfile, self.Q_table)
        return

    def load_model(self):
        with open('test.npy', 'rb') as model_file:
            self.Q_table = np.load(model_file)

        print(self.Q_table)
        return

    def print_model(self):
        print(self.Q_table)


def test():
    model = DeepNet()
    model.create_game()
    print('-------------------')


if __name__ == "__main__":
    test()
