
class Position:
    x = 0
    y = 0
    complete = 0


    def __init__(self, x, y):
        self.x = x
        self.y = y
        return

    def is_complete(self):
        return self.complete == 1

    def set_complete(self):
        self.complete = 1

    def set_target(self):
        self.complete = 0 
        
    def __lt__(self,other):
        return self.x*self.y < other.x*other.y

    def __eq__(self, other):
        return self.x == other.x\
            and self.y == other.y

    def __hash__(self):
        return hash(('x', self.x,
                     'y', self.y,
                     'complere',self.complete))

    def __repr__(self):
        return "(x: {}, y: {}) = {} ".format(self.x, self.y, self.complete)

class State:

    r1:Position = None
    b1:Position = None
    g1:Position = []
    holding_location:Position = None
    pickup_location:Position = None

    def __init__(self, r1, b1, g1, holding_location, pickup_location):
        self.r1 = r1
        self.b1 = b1
        self.g1 = g1
        self.holding_location = holding_location
        self.pickup_location = pickup_location
        return

    def is_holding(self):
        return self.b1 == self.holding_location

    def set_holding(self):
        self.b1 = self.holding_location

    def drop(self):
        self.b1 = self.pickup_location


    def __eq__(self, other):
        return self.r1 == other.r1\
            and self.b1 == other.b1\
            and self.g1 == other.g1

    def __hash__(self):
        return hash(('robot', self.r1,
                     'brick', self.b1,
                     'goal', tuple(self.g1)))

    def __repr__(self):
        print(self.r1, self.b1, self.g1)
        return super().__repr__()

