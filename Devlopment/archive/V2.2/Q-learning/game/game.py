import random
import numpy as np
from copy import deepcopy
from itertools import combinations as ncr  
try:
    from game.state import State, Position
    from game.combi import combinations
except Exception:
    from state import State, Position
    from combi import combinations

"""
state representation
[0,0,0,1,0,0,0]

G = target
B = brick

actions = [
    left, right, pick up, drop down
] len = 4
"""


class Game:

    shape = (0,0)
    target_size = 2
    
    # the formation of the goal brick layout
    current_state:State = None

    states = None

    actions = [0, 0, 0, 0, 0, 0]

    def __init__(self, shape, do_gen = True):

        self.shape = shape

        brick_locations = [Position(0,0), Position(-1,-1)]

        locations = []

        for rx in range(self.shape[0]):
                for ry in range(self.shape[1]):
                    locations.append(Position(rx,ry))

        perms = ncr(range( len(locations)), self.target_size) 
        perms = [ [deepcopy(locations[x]) for x in p] for p in perms]


        if do_gen:
            states = []
            #robot locations
            for rp in locations:
                #brick locations
                for bp in brick_locations:
                    #potential goalstates
                    for p in perms:
                        for gs in combinations(p):
                            #each possible state
                            new_state = State(
                                deepcopy(rp),
                                deepcopy(bp),
                                deepcopy(gs),
                                deepcopy(brick_locations[1]),
                                deepcopy(brick_locations[0]))
                            states.append(new_state)      
        
            # self.states = states
            self.states = tuple(states)


            print(len(states))
            

            np.save('states_data', self.states, allow_pickle=True)
        
        else:
            self.states = list(np.load('states_data.npy', allow_pickle=True))


        print(len(self.states))

        self.reset()

        return

    def saved_state(self):
        return tuple(list(np.load('states_data.npy', allow_pickle=True)))

    def reset(self):
        loop= True
        while loop:
            self.current_state = deepcopy(random.choice(self.states))
            for i in self.current_state.g1:
                if not i.complete:
                    loop = 0
                    break

        return

    # def get_actions(self):
    #     self.actions[0] = [1, 0, 0, 0] if self.r1-1 >= 0 else [0,  0, 0, 0]
    #     self.actions[1] = [0, 1, 0, 0] if self.r1 + 1 < len(self.goal_state) else [0, 0, 0, 0]
    #     self.actions[2] = [0, 0, 1, 0] if not self.is_holding and self.b1 == self.r1 else [0, 0, 0, 0]
    #     self.actions[3] = [0, 0, 0, 1] if self.is_holding is True else [0, 0, 0, 0]
    #     return self.actions

    def get_reward(self, action):

        if(action == 0):
            # print('left')
            if self.current_state.r1.x-1 >= 0:
                return (-1, False, False)
            else:
                return (-10, True, False)

        if(action == 1):
            # print('right')
            if self.current_state.r1.x + 1 < self.shape[1]:
                return (-1, False, False)
                
            else:
                return (-10, True, False)

        
        if(action == 2):
            # print('up')
            if self.current_state.r1.y - 1 >= 0:
                return (-1, False, False)
                
            else:
                return (-10, True, False)


        if(action == 3):
            # print('down')
            if self.current_state.r1.y + 1 < self.shape[0]:
                return (-1, False, False)
                
            else:
                return (-10, True, False)

        

        if(action == 4):
            # print('pick')
            if not self.current_state.is_holding() and self.current_state.b1 == self.current_state.r1:
                return (-1, False, False)
            else:
                return (-10, True, False)

        if(action == 5):
            # print('drop')
            # print()
            if self.current_state.is_holding():
                remain  = [location for location in self.current_state.g1 if location.complete == 0]
                try:
                    remain.index(self.current_state.r1)
                    if len(remain)==1:
                        print("----------------------------SUCCESS")
                        return (200, True, True)
                    return (100, False, False)

                except Exception:
                    return (-10, True, False)

            else:
                return (-10, True, True)

        raise Exception('Incorrect actions parsed {}'.format(action))
        return

    def update_state(self, action):

        state = deepcopy(self.current_state)

        if(action == 0):
            state.r1.x += -1
 
        if(action == 1):
            state.r1.x += 1

        if(action == 2):
            state.r1.y += -1
 
        if(action == 3):
            state.r1.y += 1

        if(action == 4):
            state.set_holding()

        if(action == 5):
            state.drop()

        self.current_state = state
        return

    def print_state(self):
        temp = np.zeros(self.shape, dtype=int).astype(str)

        r = self.current_state.r1
        b = self.current_state.b1
        g1 = self.current_state.g1



        if self.current_state.is_holding():
            temp[r.y][r.x] = 'H'

        elif self.current_state.b1 == self.current_state.r1:
            temp[b.y][b.x] = 'BR'

        else:
            temp[r.y][r.x] = 'R'
            temp[b.y][b.x] = 'B'

        
        for g in g1:
            if temp[g.y][g.x] == '0':
                temp[g.y][g.x] = 'G' if g.complete == 0 else 'X'
            else:
                temp[g.y][g.x] += ' G' if g.complete == 0 else 'X'

        print(temp)
        print(g)
        # self.current_state.__repr__()
        return

    def do_action(self, action):

        reward, done, is_success = self.get_reward(action)

        if not done:
            self.update_state(action)

        state_index = self.get_state_index()
        return (state_index, reward, done, is_success)

    def get_state_index(self):
        
        try:
            index = self.states.index(self.current_state)
        except:
            print(self.current_state)
            print(self.states == self.saved_state())
        
        
        return index


def test():
    game = Game(5)

    game.print_state()

    print('-------------------')

    game.print_state()


if __name__ == "__main__":
    test()
