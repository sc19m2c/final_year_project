import numpy as np
from copy import deepcopy

def recursive(state):

    l = [state]
    
    for i in range(len(state)):
        new_s = deepcopy(state)
        new_s[i].set_complete()
        l.append(new_s)

        h1 = new_s[0:i+1]
        h2 = new_s[i+1:]

        # print(state)

        # print(new_s)

        # print(h1,h2)
        # raise Exception

        if len(h2) == 0:
            return l

        combinations = recursive(h2)

        for j in range(len(combinations)):
            s = h1+combinations[j]
            l.append(s)

    return l

def combinations(state):

    combi = np.array(recursive(state))
    b_set = set(map(tuple,combi))  #need to convert the inner lists to tuples so they are hashable
    b = [ list(x) for x in b_set ]
    return b




def test():

    state  = [-1,-2, -3, -4, -5]

    combi = np.array(recursive(state))

    uni = np.unique(combi, axis=0)

    print(uni)
    print(len(uni))

    return




if __name__ == "__main__":
    test()