import os.path
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

try:
    from game.state import State, Position
except Exception:
    from .state import State, Position

import random
import numpy as np

"""
state representation
[0,0,0,1,0,0,0]

G = target
B = brick

actions = [
    left, right, pick up, drop down
] len = 4
"""


class Game:

    shape = (0,0)

    # the formation of the goal brick layout
    current_state:State = None
    pickup_location:Position = None
    holding_location:Position = None
   
    states = None

    actions = np.array([
        [1, 0, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 0, 1, 0, 0, 0],
        [0, 0, 0, 1, 0, 0],
        [0, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 0, 1]
    ])

    def __init__(self, shape):

        self.shape = shape
        self.pickup_location = Position(0,0)
        self.holding_location = Position(self.shape[0]-1,self.shape[1])
        self.reset()

        return

    def reset(self):

        s = True
        while s:
            num_targets = random.randint(2, int(self.shape[0]**2/2))

            target_locations = []

            i = 0
            while i < num_targets:
                while True:
                    new_pos = Position(random.randint(0, self.shape[0]-1),random.randint(0, self.shape[1]-1))
                    try:
                        target_locations.index(new_pos)
                        continue
                    except Exception:
                        target_locations.append(new_pos)
                        i += 1
                        break
                        

            for pos in target_locations:
                pos.is_complete = random.choice([0,1])

            for pos in target_locations:
                if not pos.is_complete_():
                    s = False
                    break

        self.current_state = State(
            Position(random.randint(0, self.shape[0]-1),random.randint(0, self.shape[1]-1)),
            self.pickup_location,
            target_locations,
            self.holding_location,
            self.pickup_location
        )

        


        return

    def get_reward(self, action, do_log):

        if((action == self.actions[0]).all()):
            # if do_log : print('left')
            if self.current_state.r1.x-1 >= 0:
                return (-2, False, False)
            else:
                return (-20, True, False)

        if((action == self.actions[1]).all()):
            # if do_log : print('right')
            if self.current_state.r1.x + 1 < self.shape[1]:
                return (-2, False, False)
            else:
                return (-20, True, False)

        if((action == self.actions[2]).all()):
            # if do_log : print('up')
            if self.current_state.r1.y - 1 >= 0:
                return (-2, False, False)
                
            else:
                return (-20, True, False)


        if((action == self.actions[3]).all()):
            # if do_log : print('down')
            if self.current_state.r1.y + 1 < self.shape[0]:
                return (-2, False, False)
                
            else:
                return (-20, True, False)

        if((action == self.actions[4]).all()):
            # if do_log : print('pick')
            if not self.current_state.is_holding() and self.current_state.b1 == self.current_state.r1:
                return (10, False, False)
            else:
                return (-30, True, False)

        if((action == self.actions[5]).all()):
            # if do_log : print('drop')
            # if do_log : print()
            if self.current_state.is_holding():
                remain  = [location for location in self.current_state.g1 if location.is_complete == 0]
                try:
                    remain.index(self.current_state.r1)
                    if len(remain)==1:
                        print("---------------------------------------------------------------------------- SUCCESS -----------------------")
                        return (200, True, True)
                    # print("-----------ok-----------")
                    return (100, False, False)

                except Exception:
                    return (-10, True, False)

            else:
                return (-30, True, True)

        raise Exception('Incorrect actions parsed {}'.format(action))
        return

    def update_state(self, action):

        if((action == self.actions[0]).all()):
            self.current_state.r1.x += -1

        if((action == self.actions[1]).all()):
            self.current_state.r1.x += 1
        
        if((action == self.actions[2]).all()):
            self.current_state.r1.y += -1
 
        if((action == self.actions[3]).all()):
            self.current_state.r1.y += 1

        if((action == self.actions[4]).all()):
            self.current_state.set_holding()

        if((action == self.actions[5]).all()):
            for g in self.current_state.g1:
                if g == self.current_state.r1:
                    g.set_complete()
            self.current_state.drop()
         
        return

    def print_state(self):
        temp = np.zeros(self.shape, dtype=int).astype(str)

        r = self.current_state.r1
        b = self.current_state.b1
        g1 = self.current_state.g1



        if self.current_state.is_holding():
            temp[r.y][r.x] = 'H'

        elif self.current_state.b1 == self.current_state.r1:
            temp[b.y][b.x] = 'BR'

        else:
            temp[r.y][r.x] = 'R'
            temp[b.y][b.x] = 'B'

        
        for g in g1:
            if temp[g.y][g.x] == '0':
                temp[g.y][g.x] = 'G' if g.is_complete == 0 else 'X'
            else:
                temp[g.y][g.x] += ' G' if g.is_complete == 0 else 'X'

        print(temp)
        return

    def do_action(self, action, do_log = False):

        reward, done, is_success = self.get_reward(action, do_log)

        if not done:
            self.update_state(action)

        state = self.get_state()
        return (state, reward, done, is_success)

    def get_state(self):
        return self.current_state.as_inputs(self.shape)

    def get_completeness(self):
        count = 0
        for g in self.current_state.g1:
            if g.is_complete_():
                count += 1
        
        return count/len(self.current_state.g1)

def test():
    game = Game((5,5))

    game.print_state()

    print('-------------------')

    game.print_state()


if __name__ == "__main__":
    test()
