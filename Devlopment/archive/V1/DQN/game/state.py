import numpy as np


class State:

    r1 = 0
    b1 = 0
    g1 = 0
    holding_location = 0

    def __init__(self, r1, b1, g1, holding_location):
        self.r1 = r1
        self.b1 = b1
        self.g1 = g1
        self.holding_location = holding_location
        return

    def is_holding(self):
        return self.b1 == self.holding_location

    def as_inputs(self):
        inputs = np.array([self.r1, self.b1, self.g1])
        return inputs

    def __eq__(self, other):
        return self.r1 == other.r1\
            and self.b1 == other.b1\
            and self.g1 == other.g1

    def __hash__(self):
        return hash(('robot', self.r1,
                     'brick', self.b1,
                     'goal', self.g1))

    def __repr__(self):
        print(self.r1, self.b1, self.g1)
        return super().__repr__()


def test():
    return

if __name__ == "__main__":
    test()