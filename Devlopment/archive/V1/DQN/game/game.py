import sys
sys.path.append('../')

from game.state import State

import random
import numpy as np

"""
state representation
[0,0,0,1,0,0,0]

G = target
B = brick

actions = [
    left, right, pick up, drop down
] len = 4
"""


class Game:

    size = None

    # the formation of the goal brick layout
    goal_state = None
    current_state = None

    states = None

    actions = np.array([
        [1, 0, 0, 0],
        [0, 1, 0, 0],
        [0, 0, 1, 0],
        [0, 0, 0, 1]
    ])

    def __init__(self, size):

        self.size = size
        self.reset()

        return

    def reset(self):

        self.current_state = State(
            random.randint(0, self.size-1),
            random.randint(0, self.size-1),
            random.randint(0, self.size-1),
            self.size
        )

        self.goal_state = self.current_state.g1
    
        states = []

        for r in range(self.size):
            for g in range(self.size):
                for b in range(self.size+1):
                    new_state = State(r, b, g, self.size)
                    states.append(new_state)

        self.states = list(sorted(set(states), key=states.index))

        return

    def get_actions(self):
        # self.actions[0] = [1, 0, 0, 0] if self.current_state.r1-1 >= 0 else [0,  0, 0, 0]
        # self.actions[1] = [0, 1, 0, 0] if self.current_state.r1 + 1 < self.size else [0, 0, 0, 0]
        # self.actions[2] = [0, 0, 1, 0] if not self.current_state.is_holding and self.current_state.b1 == self.current_state.r1 else [0, 0, 0, 0]
        # self.actions[3] = [0, 0, 0, 1] if self.current_state.is_holding else [0, 0, 0, 0]
        return self.actions

    def get_reward(self, action):

        if((action == self.actions[0]).all()):
            # print('left')
            if self.current_state.r1-1 >= 0:
                return (-2, False, False)
            else:
                return (-20, True, False)

        if((action == self.actions[1]).all()):
            # print('right')
            if self.current_state.r1 + 1 < self.size:
                return (-2, False, False)
            else:
                return (-20, True, False)

        if((action == self.actions[2]).all()):
            # print('pick')
            if not self.current_state.is_holding() and self.current_state.b1 == self.current_state.r1:
                return (-2, False, False)
            else:
                return (-10, True, False)

        if((action == self.actions[3]).all()):
            # print('drop')
            if self.current_state.is_holding() and self.current_state.r1 == self.goal_state:
                print("---------------------------------------------------------------------------- SUCCESS -----------------------")
                return (100, True, True)
            else:
                return (-10, True, False)

        raise Exception('Incorrect actions parsed {}'.format(action))

    def update_state(self, action):

        if((action == self.actions[0]).all()):
            self.current_state.r1 += -1

        if((action == self.actions[1]).all()):
            self.current_state.r1 += 1

        if((action == self.actions[2]).all()):
            self.current_state.b1 = self.size

        if((action == self.actions[3]).all()):
            self.current_state.b1 = self.current_state.r1
        return

    def print_state(self):
        temp = ['0']*self.size
        if self.current_state.is_holding():
            temp[self.current_state.r1] = 'h'

        elif self.current_state.b1 == self.current_state.r1:
            temp[self.current_state.b1] = 'bf1'

        else:
            temp[self.current_state.r1] = 'r1'
            temp[self.current_state.b1] = 'b1'

        temp[self.current_state.g1] += ' G'
        print(temp)
        self.current_state.__repr__()
        return

    def print_action(self):
        print(self.get_actions())
        return

    def do_action(self, action):

        reward, done, is_success = self.get_reward(action)

        if not done:
            self.update_state(action)
        else:
            print('::::: game over :::::')
        return (self.current_state.as_inputs(), reward, done, is_success)

    def get_state_index(self):
        index = self.states.index(self.current_state)
        return index

    def get_state(self):
        return self.current_state.as_inputs()


def test():
    game = Game(5)

    game.print_state()

    print('-------------------')

    game.print_state()


if __name__ == "__main__":
    test()
