import sys
sys.path.append('../')

from game.game import Game

def main():
    game = Game(5)

    while True:
        game.print_state()
        action = input("Enter action:")
        state, reward, done = game.do_action(int(action))

        if(done):
            break
    return


if __name__ == "__main__":
    main()
