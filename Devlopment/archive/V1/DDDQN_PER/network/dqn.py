import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import tensorflow as tf
tf.logging.set_verbosity(tf.logging.ERROR)

class DQNetwork:
    def __init__(self, state_size, action_size, learning_rate, name='DQNetwork'):
        self.state_size = state_size
        self.action_size = action_size
        self.learning_rate = learning_rate

        with tf.variable_scope(name):
   
            self.inputs_ = tf.placeholder(tf.float32, [None, *state_size], name="inputs")
            self.actions_ = tf.placeholder(tf.float32, [None, *action_size], name="actions_")
            self.ISWeights_ = tf.placeholder(tf.float32, [None,1], name='IS_weights')
   
            self.target_Q = tf.placeholder(tf.float32, [None], name="target")
            self.l1 = tf.layers.dense(inputs=self.inputs_,
                                      units=3,
                                      activation=tf.nn.elu,
                                      kernel_initializer=tf.contrib.layers.xavier_initializer(),
                                      name="l1")

            self.l2 = tf.layers.dense(inputs=self.l1,
                                      units=10,
                                      activation=tf.nn.elu,
                                      kernel_initializer=tf.contrib.layers.xavier_initializer(),
                                      name="l2")


            self.l3 = tf.layers.dense(inputs=self.l2,
                                      units=20,
                                      activation=tf.nn.elu,
                                      kernel_initializer=tf.contrib.layers.xavier_initializer(),
                                      name="l3")

            self.value_fc = tf.layers.dense(inputs=self.l3,
                                      units=10,
                                      activation=tf.nn.elu,
                                      kernel_initializer=tf.contrib.layers.xavier_initializer(),
                                      name="value_fc")

            self.value = tf.layers.dense(inputs=self.value_fc,
                                          kernel_initializer=tf.contrib.layers.xavier_initializer(),
                                          units=1,
                                          activation=None,
                                          name = "value")


            self.action_fc = tf.layers.dense(inputs=self.l3,
                                      units=10,
                                      activation=tf.nn.elu,
                                      kernel_initializer=tf.contrib.layers.xavier_initializer(),
                                      name="action_fc")

   
            self.action = tf.layers.dense(inputs=self.action_fc,
                                          kernel_initializer=tf.contrib.layers.xavier_initializer(),
                                          units=self.action_size[0],
                                          activation=None,
                                          name="action")

            self.output = self.value + tf.subtract(self.action, tf.reduce_mean(self.action, axis=1, keepdims=True))

            self.Q = tf.reduce_sum(tf.multiply(self.output, self.actions_), axis=1)
            
     
            self.absolute_errors = tf.abs(self.target_Q - self.Q)
            
            self.loss = tf.reduce_mean(self.ISWeights_ * tf.squared_difference(self.target_Q, self.Q))

            self.optimizer = tf.train.RMSPropOptimizer(self.learning_rate).minimize(self.loss)
