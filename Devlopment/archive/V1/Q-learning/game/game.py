import random
try:
    from game.state import State
except Exception:
    from state import State

"""
state representation
[0,0,0,1,0,0,0]

G = target
B = brick

actions = [
    left, right, pick up, drop down
] len = 4
"""


class Game:

    size = None

    # the formation of the goal brick layout
    goal_state = None
    current_state = None

    states = None

    actions = [0, 0, 0, 0]

    def __init__(self, size):

        self.size = size
        self.reset()

        return

    def reset(self):

        self.current_state = State(
            random.randint(0, self.size-1),
            random.randint(0, self.size-1),
            random.randint(0, self.size-1),
            self.size
        )

        self.goal_state = self.current_state.g1

        states = []

        for r in range(self.size):
            for g in range(self.size):
                for b in range(self.size+1):
                    new_state = State(r, b, g, self.size)
                    states.append(new_state)

        self.states = list(sorted(set(states), key=states.index))

        return

    # def get_actions(self):
    #     self.actions[0] = [1, 0, 0, 0] if self.r1-1 >= 0 else [0,  0, 0, 0]
    #     self.actions[1] = [0, 1, 0, 0] if self.r1 + 1 < len(self.goal_state) else [0, 0, 0, 0]
    #     self.actions[2] = [0, 0, 1, 0] if not self.is_holding and self.b1 == self.r1 else [0, 0, 0, 0]
    #     self.actions[3] = [0, 0, 0, 1] if self.is_holding is True else [0, 0, 0, 0]
    #     return self.actions

    def get_reward(self, action):

        if(action == 0):
            print('left')
            if self.current_state.r1-1 >= 0:
                return (-1, False, False)
            else:
                return (-10, True, False)

        if(action == 1):
            print('right')
            if self.current_state.r1 + 1 < self.size:
                return (-1, False, False)
                
            else:
                return (-10, True, False)

        if(action == 2):
            print('pick')
            if not self.current_state.is_holding() and self.current_state.b1 == self.current_state.r1:
                return (-1, False, False)
            else:
                return (-10, True, False)

        if(action == 3):
            print('drop')
            print()
            if self.current_state.is_holding() and self.current_state.r1 == self.goal_state:
                print("SUCCESS")
                return (100, True, True)
            else:
                return (-10, True, False)

        raise Exception('Incorrect actions parsed {}'.format(action))
        return

    def update_state(self, action):

        if(action == 0):
            self.current_state.r1 += -1
 
        if(action == 1):
            self.current_state.r1 += 1

        if(action == 2):
            self.current_state.b1 = self.size

        if(action == 3):
            self.current_state.b1 = self.current_state.r1
        return

    def print_state(self):
        temp = ['0']*self.size
        if self.current_state.is_holding():
            temp[self.current_state.r1] = 'h'

        elif self.current_state.b1 == self.current_state.r1:
            temp[self.current_state.b1] = 'bf1'

        else:
            temp[self.current_state.r1] = 'r1'
            temp[self.current_state.b1] = 'b1'

        temp[self.current_state.g1] += ' G'
        print(temp)
      
        return

    def do_action(self, action):

        reward, done, is_success = self.get_reward(action)

        if not done:
            self.update_state(action)

        state_index = self.get_state_index()
        return (state_index, reward, done, is_success)

    def get_state_index(self):
        index = self.states.index(self.current_state)
        return index


def test():
    game = Game(5)

    game.print_state()

    print('-------------------')

    game.print_state()


if __name__ == "__main__":
    test()
