

# from learning.memory import Memory


# mem = Memory(100000)
# mem.load_memory()
# mem.print()

# sample = mem.sample(500)
# print(sample)
from game.game import Game
import numpy as np

def test_accept_bp():
    game = Game((5,5))
    game.reset()
    game.print_state()

    goal = np.array([
        [0,0,0,0,1],
        [0,0,0,0,1],
        [0,0,0,0,1],
        [0,0,0,0,1],
        [0,0,0,0,1],
    ])
    game.current_state.set_goal_state(goal)

    game.print_state()
    
    return 


if __name__ == "__main__":
    test_accept_bp()