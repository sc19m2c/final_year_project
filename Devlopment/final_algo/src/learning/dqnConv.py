import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import tensorflow as tf
tf.logging.set_verbosity(tf.logging.ERROR)

class DDDQN:
    def __init__(self, state_size, action_size, learning_rate, name='DDDQN'):
        self.state_size = state_size
        self.action_size = action_size
        self.learning_rate = learning_rate

        with tf.variable_scope(name):
        
            print([None, *state_size, 1])
            self.inputs_ = tf.placeholder(tf.float32, [None, *state_size], name="inputs")
            self.actions_ = tf.placeholder(tf.float32, [None, *action_size], name="actions_")
            self.ISWeights_ = tf.placeholder(tf.float32, [None,1], name='IS_weights')
            
            self.target_Q = tf.placeholder(tf.float32, [None], name="target")

            #input layer - conv 
            self.c1 = tf.layers.conv2d(inputs = self.inputs_,
                                         filters = 32,
                                         kernel_size = [2,2],
                                         strides = [1,1],
                                         padding = "VALID",
                                          kernel_initializer=tf.contrib.layers.xavier_initializer_conv2d(),
                                         name = "c1")

            self.c1_out = tf.nn.elu(self.c1, name="c1_out")

            #second layer - conv 
            self.c2 = tf.layers.conv2d(inputs = self.c1_out,
                                 filters = 64,
                                 kernel_size = [2,2],
                                 strides = [1,1],
                                 padding = "VALID",
                                 kernel_initializer=tf.contrib.layers.xavier_initializer_conv2d(),
                                 name = "c2")

            self.c2_out = tf.nn.elu(self.c2, name="c2_out")

            #third layer - conv 
            self.c3 = tf.layers.conv2d(inputs = self.c2_out,
                                 filters = 128,
                                 kernel_size = [1,1],
                                 strides = [1,1],
                                 padding = "VALID",
                                kernel_initializer=tf.contrib.layers.xavier_initializer_conv2d(),
                                 name = "c3")

            self.c3_out = tf.nn.elu(self.c3, name="c3_out")
            
            
            #flatten the conv
            self.flatten = tf.layers.flatten(self.c3_out)

            #value stream - fully connected
            self.value_fc = tf.layers.dense(inputs=self.flatten,
                                      units=512,
                                      activation=tf.nn.elu,
                                      kernel_initializer=tf.contrib.layers.xavier_initializer(),
                                      name="value_fc")

            #value stream out put layer
            self.value = tf.layers.dense(inputs=self.value_fc,
                                          kernel_initializer=tf.contrib.layers.xavier_initializer(),
                                          units=1,
                                          activation=None,
                                          name = "value")

            #action stream fully connected
            self.action_fc = tf.layers.dense(inputs=self.flatten,
                                      units=512,
                                      activation=tf.nn.elu,
                                      kernel_initializer=tf.contrib.layers.xavier_initializer(),
                                      name="action_fc")

            #action stream output
            self.action = tf.layers.dense(inputs=self.action_fc,
                                          kernel_initializer=tf.contrib.layers.xavier_initializer(),
                                          units=self.action_size[0],
                                          activation=None,
                                          name="action")

            
            # special agregation layers dueling network
            self.output = self.value + tf.subtract(self.action, tf.reduce_mean(self.action, axis=1, keepdims=True))

            # Qvlaues as the output of the network
            self.Q = tf.reduce_sum(tf.multiply(self.output, self.actions_), axis=1)
            
            # returned for PER priorty scoring
            self.absolute_errors = tf.abs(self.target_Q - self.Q)
            
            # implenets PER features, using IS 
            self.loss = tf.reduce_mean(self.ISWeights_ * tf.squared_difference(self.target_Q, self.Q))

            # network optimizer
            self.optimizer = tf.train.RMSPropOptimizer(self.learning_rate).minimize(self.loss)
