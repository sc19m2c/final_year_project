import numpy as np



class Metrics:

    def __init__(self):

        #(loss, t step)
        self.loss = []
        
        #(%, epsiode)
        self.completeness = []
        
        #(rpg, episode )
        self.rewards_per_goal = []

        #(conf rate, episode )
        #test every 1000 eps for 100 eps
        self.confusion = []

        #(succ rate,episode)
        #test every 1000 eps for 100 eps
        self.success_rate = []
        return

    
    def update_loss(self, loss, train_step):
        self.loss.append((loss, train_step))

    def update_complteness(self, completeness, ep):
        self.completeness.append((completeness, ep))

    def update_rewards(self, rpg, ep):
        self.rewards_per_goal.append((rpg, ep))

    def update_confusion(self, confusion, ep):
        self.confusion.append((confusion, ep))

    def update_success_rate(self, success, ep):
        self.success_rate.append((success, ep))


    def save_all(self):

        prefix = "./metrics/data/"
    
        np.save(prefix+'loss', self.loss, allow_pickle=True)
        np.save(prefix+'completeness', self.completeness, allow_pickle=True)
        np.save(prefix+'rewards_per_goal', self.rewards_per_goal, allow_pickle=True)
        np.save(prefix+'confusion', self.confusion, allow_pickle=True)
        np.save(prefix+'success_rate', self.success_rate, allow_pickle=True)

        return

        