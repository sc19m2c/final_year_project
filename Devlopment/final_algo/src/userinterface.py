import sys
sys.path.append('../')

try:
    from game.game import Game
except Exception:
    from game import Game


def main():
    game = Game((5,5))

    while True:
        game.print_state()
        print(game.get_state())
        action = input("Enter action:")
        
        state, reward, done, is_success = game.do_action(game.actions[int(action)])
        print(reward)
        if(done):
            break
    return


if __name__ == "__main__":
    main()
