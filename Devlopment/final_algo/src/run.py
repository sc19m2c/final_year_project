import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

from learning.learn import DeepNet
import numpy as np

model = DeepNet()
# model.memory.load_memory()
model.init_memory_data()
# print(model.memory.tree.data[0])
# model.memory.save_memory()


# print('----------------------- PRETRAIN COMPELTE')

# # # model = DeepNet()

# # model.memory.load_memory()
# # # model2.memory.print()
model.train()


# model.play(1000)
# bp = np.array([
#     [0,0,1,0,0],
#     [0,0,0,1,0],
#     [0,0,0,0,1],
#     [0,0,0,1,1],
#     [0,0,0,1,1],
# ])

# plan = model.gen_action_plan(bp)
# print(plan)