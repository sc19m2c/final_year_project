import csv  
import time


class Logger:

    
    def __init__(self):
        #open a new file and keep file open
        self.filename = "./util/logs/training_session_" + str(int(time.time())) +".csv"
        self.file = open(self.filename, 'w', newline='')
        self.writer = csv.writer(self.file)
        return


    def log(self, episode, rewards, loss, eps):
        #append data to file
        self.writer.writerow([episode,rewards,loss, eps])
        self.file.flush()
        return