
import numpy as np
from copy import deepcopy
try:
    from game.state import State, Position
except Exception:
    from state import State, Position


#rewards
STEP = -2
ERROR_STEP = -20

PICK = 10
ERROR_PICK = -30

DROP_PARTIAL = 100
DROP_COMPLETE = 500
DROP_ERROR_MINOR = -10
DROP_ERROR_MAJOR = -30


class Action:

    actions = actions = np.array([
        #navigation
        [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  #nav_left
        [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  #nav_right  
        [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  #nav_up
        [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  #nav_down

        #pick
        [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],  #pick_left
        [0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],  #pick_up
        [0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],  #pick_up_left

        #drop
        [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],  #drop_left
        [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0],  #drop_right
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],  #drop_up
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0],  #drop_down
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],  #drop_up_left
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0],  #drop_up_right
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],  #drop_down_left
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],  #drop_down_right
   
    ])

    #string name of each actions
    action_name_space = ["nav_left","nav_right","nav_up","nav_down","pick_left","pick_up","pick_up_left","drop_left","drop_right","drop_up","drop_down","drop_up_left","drop_up_right","drop_down_left","drop_down_right"]

    action_choice_name = ""


    def __init__(self):
        return

    
    def get_action_name(self,index):
        #return the string name of an action
        return self.action_name_space[index]
         

    
    def set_action_name(self, action_choice):
        #set the string name of the action to a class varriable
        index = [i for i,x in enumerate(self.actions) if (x == action_choice).all()][0] 
        self.action_choice_name = self.action_name_space[index]
        return


    def is_location_filled(self, pos, current_state):
        #determines if an location in the environment conatins an obstacle
        filled = [location for location in current_state.g1 if location.is_complete == 1]
        try:
            filled.index(pos)
            return True
        except Exception:
            return False

    def get_reward_update_state(self, action_choice, current_state, shape):
        #contains the game logic and rewards policy
        self.set_action_name(action_choice)

        #create variables of the current state
        new_state:State  = deepcopy(current_state)
        action_type = self.action_choice_name.split("_")[0]
        r = current_state.r1 #robot location
        p = current_state.pickup_location #pickup location

        # navigation rewards --------------------------------------
    
        if action_type == "nav":
            # returns (rewards, is done, is success, state)
            nav_success = (STEP, False, False, new_state)
            nav_error = (ERROR_STEP, True, False, new_state)

            if self.action_choice_name == "nav_left":
                if r.x-1 >= 0:
                    n = Position(current_state.r1.x-1, current_state.r1.y)
                    #navigates into obstacles
                    if self.is_location_filled(n, current_state):
                        return nav_error

                    new_state.r1.x += -1
                    return nav_success
            
            if self.action_choice_name == "nav_right":
                if r.x + 1 < shape[1]:
                    n = Position(current_state.r1.x+1, current_state.r1.y)
                    #navigates into obstacles
                    if self.is_location_filled(n, current_state):
                        return nav_error

                    new_state.r1.x += 1
                    return nav_success

            if self.action_choice_name == "nav_up":
                if r.y - 1 >= 0:
                    n = Position(current_state.r1.x, current_state.r1.y-1)
                    #navigates into obstacles
                    if self.is_location_filled(n, current_state):
                        return nav_error

                    new_state.r1.y += -1
                    return nav_success

            if self.action_choice_name == "nav_down":
                if r.y + 1 < shape[0]:
                    n = Position(current_state.r1.x, current_state.r1.y+1)
                    #navigates into obstacles
                    if self.is_location_filled(n, current_state):
                        return nav_error

                    new_state.r1.y += 1
                    return nav_success

            #navigates outside of the environment
            return nav_error


        # pick rewards --------------------------------------

        if action_type == "pick":
            # returns (rewards, is done, is success, state)
            pick_success = (PICK, False, False, new_state)
            pick_error = (ERROR_PICK, True, False, new_state)

            if current_state.is_holding():
                #cannot hold a brick when trying to pick a brick
                return pick_error
            
            else:
                if self.action_choice_name == "pick_up":
                    if r.y == p.y + 1 and r.x == p.x:
                        new_state.set_holding()
                        return pick_success
                    
                if self.action_choice_name == "pick_left":
                    if r.y == p.y and r.x == p.x + 1:
                        new_state.set_holding()
                        return pick_success
                 
                if self.action_choice_name == "pick_up_left":
                    if r.y == p.y + 1 and r.x == p.x + 1:
                        new_state.set_holding()
                        return pick_success

                #error action unknow
                return pick_error


        # drop rewards --------------------------------------

        if action_type == "drop":
            # returns (rewards, is done, is success, state)
            drop_success_partial = (DROP_PARTIAL, False, False, new_state)
            drop_success_complete = (DROP_COMPLETE, True, True, new_state)
            drop_error_minor = (DROP_ERROR_MINOR, True, False, new_state)
            drop_error_major = (DROP_ERROR_MAJOR, True, False, new_state)

            if not current_state.is_holding():
                #must be holding a brick to place a brick
                return drop_error_major

            else:
                #creates refrecne to remaining target locations
                targets = [location for location in current_state.g1 if location.is_complete == 0]
                
                def loop(condition):
                    #reuable function to determine if the action satifies any remaining target brick locations
                    for target in targets:
                        if condition(target):
                            new_state.complete_target(target)
                            new_state.drop()
                            #condition is met and last target brick location, the game is complete
                            if len(targets) == 1:
                                print("---------------------------------------- success ------------------------------------")
                                return drop_success_complete
                            else:
                                #condition is met and remaining target brick locations
                                return drop_success_partial
                    
                    return drop_error_minor


                if self.action_choice_name == "drop_left":
                    #function condition defines the action
                    def con_dl(d):
                        if r.y == d.y and r.x == d.x + 1:
                            return True
                        else:
                            return False
                    #parse the contion to the reusable loop to cheeck if its feasible
                    return loop(con_dl)

                if self.action_choice_name == "drop_right":
                    #function condition defines the action
                    def con_dr(d):
                        if r.y == d.y and r.x == d.x - 1:
                            return True
                        else:
                            return False
                    #parse the contion to the reusable loop to cheeck if its feasible
                    return loop(con_dr)


                if self.action_choice_name == "drop_up":
                    #function condition defines the action
                    def con_du(d):
                        if r.y == d.y + 1 and r.x == d.x:
                            return True
                        else:
                            return False
                    #parse the contion to the reusable loop to cheeck if its feasible
                    return loop(con_du)

                if self.action_choice_name == "drop_down":
                    #function condition defines the action
                    def con_dd(d):
                        if r.y == d.y - 1 and r.x == d.x:
                            return True
                        else:
                            return False
                    #parse the contion to the reusable loop to cheeck if its feasible
                    return loop(con_dd)

                if self.action_choice_name == "drop_up_left":
                    #function condition defines the action
                    def con_dul(d):
                        if r.y == d.y + 1 and r.x == d.x + 1:
                            return True
                        else:
                            return False
                    #parse the contion to the reusable loop to cheeck if its feasible
                    return loop(con_dul)

                if self.action_choice_name == "drop_up_right":
                    #function condition defines the action
                    def con_dur(d):
                        if r.y == d.y + 1 and r.x == d.x - 1:
                            return True
                        else:
                            return False
                    #parse the contion to the reusable loop to cheeck if its feasible
                    return loop(con_dur)

                if self.action_choice_name == "drop_down_left":
                    #function condition defines the action
                    def con_ddl(d):
                        if r.y == d.y - 1 and r.x == d.x + 1:
                            return True
                        else:
                            return False
                    #parse the contion to the reusable loop to cheeck if its feasible
                    return loop(con_ddl)

                if self.action_choice_name == "drop_down_right":
                    #function condition defines the action
                    def con_ddr(d):
                        if r.y == d.y - 1 and r.x == d.x - 1:
                            return True
                        else:
                            return False
                    #parse the contion to the reusable loop to cheeck if its feasible
                    return loop(con_ddr)

    


def test_get_action_name():
    state = State(
        Position(4,1),
        Position(4,5),
        [Position(0,1)],
        Position(4,5),
        Position(0,0)
    )



if __name__ == "__main__":
    test_get_action_name()
