import numpy as np

class Position:
    #representation of a location of an environemnt object
    x = 0
    y = 0
    #used for bricks only
    is_complete = 0


    def __init__(self, x, y):
        self.x = x
        self.y = y
        return

    def as_inputs(self):
        #redacted
        return np.array([self.x, self.y])

    #utility function -----------------

    def is_complete_(self):
        return self.is_complete == 1

    def set_complete(self):
        self.is_complete = 1

    def set_target(self):
        self.is_complete = 0 
        
    def __lt__(self,other):
        return self.x*self.y < other.x*other.y

    def __eq__(self, other):
        return self.x == other.x\
            and self.y == other.y

    def __hash__(self):
        return hash(('x', self.x,
                     'y', self.y,
                     'complere',self.is_complete))

    def __repr__(self):
        return "(x: {}, y: {}) = {} ".format(self.x, self.y, self.is_complete)

class State:
    #representation of the state of the game
    r1:Position = None #robot location
    b1:Position = None #bricks location 
    g1:Position = [] #goal state
    holding_location:Position = None #location of holding a brick
    pickup_location:Position = None #location to pick up new bricks


    def __init__(self, r1, b1, g1, holding_location, pickup_location):
        self.r1 = r1
        self.b1 = b1
        self.g1 = g1
        self.holding_location = holding_location
        self.pickup_location = pickup_location
        return


    def as_inputs(self, shape):
        #returns the state as (5x5x2)
        state_array = np.zeros(shape)
        #channel 1 = goal state representation
        for g in self.g1:
            state_array[g.y][g.x] = -1 if not g.is_complete_() else 1

        #channel 3 = robot representation
        robot_array = np.zeros(shape)
        robot_array[self.r1.y][self.r1.x] = -1 if not self.is_holding() else 1


        states = np.array([state_array,robot_array]) 
        # Stack the frames
        stacked_state = np.stack(states, axis=2)

        return stacked_state


    
    
    def set_goal_state(self, blueprint):
        #accepts a blueprint and sets the goal state
        self.g1 = []
        for y in range(blueprint.shape[0]):
            for x in range(blueprint.shape[1]):
                if blueprint[y][x] == 1:
                    self.g1.append(Position(x,y))
        return
      

    #utility fucntion -------------------

    def is_holding(self):
        return self.b1 == self.holding_location

    def set_holding(self):
        self.b1 = self.holding_location

    def drop(self):
        self.b1 = self.pickup_location

    def complete_target(self, target):
        i = self.g1.index(target)
        self.g1[i].set_complete()



    def __eq__(self, other):
        return self.r1 == other.r1\
            and self.b1 == other.b1\
            and self.g1 == other.g1

    def __hash__(self):
        return hash(('robot', self.r1,
                     'brick', self.b1,
                     'goal', tuple(self.g1)))

    def __repr__(self):
        print(self.r1, self.b1, self.g1)
        return super().__repr__()

