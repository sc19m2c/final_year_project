import os.path
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

try:
    from game.state import State, Position
    from game.actions import Action
except Exception:
    from .state import State, Position
    from .actions import Action

import random
import numpy as np

"""
state representation
[0,0,0,1,0,0,0]

G = target
B = brick

actions = [
    left, right, pick up, drop down
] len = 4
"""


class Game:

    shape = (0,0)

    current_state:State = None
    pickup_location:Position = None
    holding_location:Position = None
   
    states = None

    def __init__(self, shape):

        #initialize the game variables
        self.shape = shape
        self.pickup_location = Position(0,0)
        self.holding_location = Position(self.shape[0]-1,self.shape[1])
        self.action_controller = Action()
        self.actions = self.action_controller.actions
        #create a new goals
        self.reset()

        return

    def reset(self):

        s = True
        #create a new game with upto 10 target brick locations
        while s:
            #determines number of target brick locations
            num_targets = random.randint(2, 10)
            target_locations = []

            i = 0
            #create the larget locations
            while i < num_targets:
                while True:
                    #create a new target location
                    new_pos = Position(random.randint(2, self.shape[0]-1),random.randint(0, self.shape[1]-1))
                    try:
                        #test if it already exits
                        target_locations.index(new_pos)
                        continue
                    except Exception:
                        #if it doesnt exist push it to the goal state
                        target_locations.append(new_pos)
                        i += 1
                        break
                        
            #ensires at least one target location is incomplete -- redunatant 
            for pos in target_locations:
                if not pos.is_complete_():
                    s = False
                    break

        #defines the current state, with default paramaters and the generated goalstate
        self.current_state = State(
            Position(0,1),
            self.pickup_location,
            target_locations,
            self.holding_location,
            self.pickup_location
        )

        return


    def print_state(self):
        #print the current state in the command line, shows target bricks and robot status
        temp = np.zeros(self.shape, dtype=int).astype(str)
        r = self.current_state.r1
        b = self.current_state.b1
        g1 = self.current_state.g1

        if self.current_state.is_holding():
            temp[r.y][r.x] = 'H'

        elif self.current_state.b1 == self.current_state.r1:
            temp[b.y][b.x] = 'BR'

        else:
            temp[r.y][r.x] = 'R'
            temp[b.y][b.x] = 'B'

        for g in g1:
            if temp[g.y][g.x] == '0':
                temp[g.y][g.x] = 'G' if g.is_complete == 0 else 'X'
            else:
                temp[g.y][g.x] += ' G' if g.is_complete == 0 else 'X'

        print(temp)
        return


    def do_action(self, action):
        #interface game provides to perform som action in the environment
        reward, done, is_success, new_state = self.action_controller.get_reward_update_state(action, self.current_state, self.shape)
        #updates the games current state
        self.current_state = new_state

        #gets the current state as inpputs for networs (5x5x2)
        state = self.get_state()

        return (state, reward, done, is_success)

    def get_state(self):
        #gets the current state as inpputs for networs (5x5x2)
        return self.current_state.as_inputs(self.shape)


    def get_completeness(self):
        count = 0
        for g in self.current_state.g1:
            if g.is_complete_():
                count += 1
        
        return count/len(self.current_state.g1)






def test():
    game = Game((5,5))

    game.print_state()

    print('-------------------')

    game.print_state()


if __name__ == "__main__":
    test()
