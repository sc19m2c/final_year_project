from re import S
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy.signal import lfilter, savgol_filter
from sklearn import preprocessing

V = "final_algo"

# Q = "./{}/Q/data/".format(V)
DQN = "./{}/DQN/data/".format(V)
DDDQN = "./{}/DDDQN/data/".format(V)

CHANNELS = "./{}/channels/data/".format(V)
ENCODED = "./{}/encoded/data/".format(V)

FINAL = "./{}/data/".format(V)

IMAGES = "./{}/images/".format(V)

PREFIXS = {
        # "Q-Learning":Q,
        # "DQN":DQN,
        # "DDDQN": DDDQN
        # "Channels":CHANNELS,
        "final": FINAL
    }

class Metrics:

    def __init__(self):

        #(loss, t step)
        self.loss = {}
        
        #(%, epsiode)
        self.completeness = {}
        
        #(rpg, episode )
        self.rewards_per_goal = {}

        #(conf rate, episode )
        #test every 1000 eps for 100 eps
        self.confusion = {}

        #(succ rate,episode)
        #test every 1000 eps for 100 eps
        self.success_rate = {}
        return

    def load_data(self, prefix = './data/'):

        for arch in PREFIXS:
            prefix = PREFIXS[arch]
            self.loss[arch] = np.load(prefix + "loss.npy", allow_pickle=True)
            self.completeness[arch]  = np.load(prefix + "completeness.npy", allow_pickle=True)
            self.rewards_per_goal[arch]  = np.load(prefix + "rewards_per_goal.npy", allow_pickle=True)
            self.confusion[arch]  = np.load(prefix + "confusion.npy", allow_pickle=True)
            self.success_rate[arch]  = np.load(prefix + "success_rate.npy", allow_pickle=True)



        return

    def modify(self):

        prefix = "./V2.2/DDDQN/data/"

        loss = np.load(prefix + "loss.npy", allow_pickle=True)
        completeness  = np.load(prefix + "completeness.npy", allow_pickle=True)
        rewards_per_goal = np.load(prefix + "rewards_per_goal.npy", allow_pickle=True)
        confusion  = np.load(prefix + "confusion.npy", allow_pickle=True)
        success_rate  = np.load(prefix + "success_rate.npy", allow_pickle=True)

        print(len(loss))

        loss = loss[:40000]

        completeness = completeness[:40000]
        rewards_per_goal = rewards_per_goal[:40000]

        confusion = confusion[:40]
        success_rate = success_rate[:40]

        np.save(prefix + 'loss', loss, allow_pickle=True)
        np.save(prefix + 'completeness', completeness, allow_pickle=True)
        np.save(prefix + 'rewards_per_goal', rewards_per_goal, allow_pickle=True)
        np.save(prefix + 'confusion', confusion, allow_pickle=True)
        np.save(prefix + 'success_rate', success_rate, allow_pickle=True)


        return

    def normalize(self, data):

        return (data - np.min(data)) / (np.max(data) - np.min(data))

    def smooth_data(self, data):
        x = 3001 if 3001 < len(data) else 7
        
        w = savgol_filter(data, x, 2)
        return w


    def plot_loss(self):

        cols = ["magenta", "orange", "blue"]

        for i, arch in enumerate(self.loss):
       
            X = np.array([x[1] for x in self.loss[arch]])
            Y = np.array([x[0] for x in self.loss[arch]])

          
            print(arch ,len(Y))
           
            df = pd.DataFrame({'ds': X, 'y': Y})

         

            rolling_mean2 = df.y.rolling(window=100).mean()
            

            rolling_mean2 = self.normalize(rolling_mean2)
        
            plt.plot(df.ds, rolling_mean2, label=arch, color=cols[i])
         

        plt.title('Normalized Loss - 100 SMA')
        plt.xlabel('episode')
        plt.ylabel('loss')

        plt.legend(loc='upper left')
        plt.savefig(IMAGES + "loss.png")
        plt.show()






    def plot_completeness(self):

        cols = ["magenta", "orange", "blue"]

        for i, arch in enumerate(self.loss):
         
            X = np.array([x[1] for x in self.completeness[arch]])
            Y = np.array([x[0] for x in self.completeness[arch]])
            # Y = self.normalize(Y)
            df = pd.DataFrame({'ds': X, 'y': Y})
     
            rolling_mean2 = df.y.rolling(window=50).mean()
            # rolling_mean2 = self.normalize(rolling_mean2)
            # plt.plot(df.ds, rolling_mean, label='AMD 50 Day SMA', color='orange')
            plt.plot(df.ds, rolling_mean2, label=arch, color=cols[i])

        plt.title('Completnes 50 SMA')
        plt.xlabel('episode')
        plt.ylabel('completness')

        plt.legend(loc='upper left')
        plt.savefig(IMAGES + "completness.png")
        plt.show()





    def plot_success_rate(self):
        cols = ["magenta", "orange", "blue"]

        for i, arch in enumerate(self.loss):
            
            X = np.array([x[1] for x in self.success_rate[arch]])
            Y = np.array([x[0] for x in self.success_rate[arch]])
            # Y = self.normalize(Y)
            plt.plot(X,Y, label=arch, color=cols[i])
        
        plt.title('Success Rate')
        plt.xlabel('episode')
        plt.ylabel('success rate')

        plt.legend(loc='upper left')
        plt.savefig(IMAGES + "success_rate.png")
        plt.show()





    def plot_confusion(self):

        cols = ["magenta", "orange", "blue"]

        for i, arch in enumerate(self.loss):
            X = np.array([x[1] for x in self.confusion[arch]])
            Y = np.array([x[0] for x in self.confusion[arch]])
            # Y = self.normalize(Y)
            plt.plot(X,Y, label=arch, color=cols[i])

        plt.title('Confusion Rate')
        plt.xlabel('episode')
        plt.ylabel('confusion Rate')

        plt.legend(loc='upper left')
        plt.savefig(IMAGES + "confussion_rate.png")
        plt.show()





    def plot_rewards(self):
        cols = ["magenta", "orange", "blue"]

        for i, arch in enumerate(self.loss):

            X = np.array([x[1] for x in self.rewards_per_goal[arch]])
            Y = np.array([x[0] for x in self.rewards_per_goal[arch]])
            # Y = self.normalize(Y)
            df = pd.DataFrame({'ds': X, 'y': Y})
        
            rolling_mean2 = df.y.rolling(window=100).mean()
            rolling_mean2 = self.normalize(rolling_mean2)
         
            plt.plot(df.ds, rolling_mean2, label=arch, color=cols[i])

        plt.title('Normalized Rewards - 100 SMA')
        plt.xlabel('episode')
        plt.ylabel('rewards')

        plt.legend(loc='upper left')
        plt.savefig(IMAGES + "rewards.png")
        plt.show()




# def test_save():
#     m = Metrics()
#     m.save_all()

def test_graphs():
    m = Metrics()
    m.load_data()
    m.plot_confusion()
    m.plot_success_rate()
    m.plot_completeness()
    m.plot_loss()
    m.plot_rewards()

    # m.modify()


if __name__ == "__main__":
    test_graphs()

        